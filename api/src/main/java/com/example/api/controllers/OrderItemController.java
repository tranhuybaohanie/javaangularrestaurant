package com.example.api.controllers;

//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.api.dao.EmployeeRepository;
import com.example.api.dao.EmployeeRepositoryCustom;
import com.example.api.dao.OrderItemRepository;
import com.example.api.dao.OrderItemRepositoryCustom;
import com.example.api.dao.OrderRepository;
import com.example.api.dao.OrderRepositoryCustom;
import com.example.api.dao.ProductCategoryRepository;
import com.example.api.dao.ProductCategoryRepositoryCustom;
import com.example.api.dao.ProductRepository;
import com.example.api.dao.ProductRepositoryCustom;
import com.example.api.model.BigCategory;
import com.example.api.model.Employee;
import com.example.api.model.FormOrderCLient;
import com.example.api.model.Order;
import com.example.api.model.OrderItem;
import com.example.api.model.Order_OrderItem;
import com.example.api.model.Product;
import com.example.api.model.ProductCategory;
import com.example.api.model.UploadFileResponse;
import com.example.api.utilities.DateTime;
import com.fasterxml.jackson.databind.node.ObjectNode;



@Controller

@RequestMapping("/order-item")
public class OrderItemController {


	
	
	@Autowired
	private OrderItemRepositoryCustom RepositoryCustom;

	@Autowired
	private OrderItemRepository Repository;



	@ResponseBody
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")

	public List<OrderItem> showAll() {
		List<OrderItem> item = this.Repository.findAll();
		return item;
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<OrderItem> GetByID(@PathVariable("id") String id) throws IOException {

		// TODO process the file here

		return ResponseEntity.ok().body(this.RepositoryCustom.getById(Long.parseLong(id)));

	}
	@RequestMapping(value = "/byorderid/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<OrderItem>> GetByIDbyorderid(@PathVariable("id") String id) throws IOException {

		// TODO process the file here

		return ResponseEntity.ok().body(this.Repository.findByorderId(Long.parseLong(id)));

	}
	

}