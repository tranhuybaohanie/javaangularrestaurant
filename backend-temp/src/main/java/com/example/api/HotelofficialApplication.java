package com.example.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.example.api.dao.UserStaffRepository;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = UserStaffRepository.class)
public class HotelofficialApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(HotelofficialApplication.class, args);
	}
}

