package com.example.api.dao;

	import java.util.List;

import com.example.api.model.UserStaff;
	 

	 
	public interface UserStaffDao {
	 
	    public void create(UserStaff car);
	 
	    public void update(UserStaff car);
	 
	    public void delete(UserStaff car);
	 
	    public void deleteAll();
	 
	    public UserStaff find(UserStaff car);
	 
	    public List < UserStaff > findAll();
	
}
