package com.example.api.dao;
import com.example.api.model.UserStaff;


import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserStaffRepository extends MongoRepository<UserStaff, String> {
	UserStaff findByEmpNo(String empNo);
	  
//	    List<UserStaff> findByFullNameLike(String fullName);
	 
//	    List<UserStaff> findByHireDateGreaterThan(Date hireDate);
	 
	    // Supports native JSON query string
//	    @Query("{fullName:'?0'}")
//	    List<UserStaff> findCustomByFullName(String fullName);
}
