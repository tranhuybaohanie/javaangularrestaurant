package com.example.api.dao;

import java.util.List;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.example.api.model.UserStaff;
 

 
@Repository
@Qualifier("UserStaffDao")
public class UserStaffDaoImpl implements UserStaffDao {
 
    @Autowired
    MongoTemplate mongoTemplate;
 
    final String COLLECTION = "UserStaff";
 
    public void create(UserStaff car) {
        mongoTemplate.insert(car);
    }
 
    public void update(UserStaff car) {
        mongoTemplate.save(car);
    }
 
    public void delete(UserStaff car) {
        mongoTemplate.remove(car);
    }
 
    public void deleteAll() {
        mongoTemplate.remove(new Query(), COLLECTION);
    }
 
    public UserStaff find(UserStaff car) {
        Query query = new Query(Criteria.where("_id").is(car.getId()));
        return mongoTemplate.findOne(query, UserStaff.class, COLLECTION);
    }
 
    public List < UserStaff > findAll() {
        return (List < UserStaff > ) mongoTemplate.findAll(UserStaff.class);
    }
 
}