package com.example.api.service;


	import java.util.List;
	 
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;

import com.example.api.dao.UserStaffDao;
import com.example.api.model.UserStaff;

	@Service("UserStaffService")
	public class UserStaffServiceImpl implements UserStaffService {
	 
	    @Autowired
	    UserStaffDao userStaffDao;
	 
	    public void create(UserStaff car) {
	    	userStaffDao.create(car);
	    }
	 
	    public void update(UserStaff car) {
	    	userStaffDao.update(car);
	    }
	 
	    public void delete(UserStaff car) {
	    	userStaffDao.delete(car);
	    }
	 
	    public List < UserStaff > findAll() {
	        return userStaffDao.findAll();
	    }
	 
	    public UserStaff find(UserStaff car) {
	        return userStaffDao.find(car);
	    }
	 
	    public void deleteAll() {
	    	userStaffDao.deleteAll();
	    }
	}
