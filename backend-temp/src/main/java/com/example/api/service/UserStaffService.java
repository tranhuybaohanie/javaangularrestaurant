package com.example.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.api.dao.UserStaffRepository;
import com.example.api.model.UserStaff;

	 
	public interface UserStaffService {
	 
	    public void create(UserStaff car);
	 
	    public void update(UserStaff car);
	 
	    public void delete(UserStaff car);
	 
	    public void deleteAll();
	 
	    public UserStaff find(UserStaff car);
	 
	    public List < UserStaff > findAll();
	
}
