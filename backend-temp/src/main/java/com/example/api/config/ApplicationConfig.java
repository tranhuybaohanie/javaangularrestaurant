package com.example.api.config;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
 
import com.mongodb.MongoClient;
 
@Configuration
@ComponentScan(basePackages = "com.example")
public class ApplicationConfig {
//	@Autowired
//	private Mongo mongo;
  //  @Bean
//    public MongoDbFactory mongoDbFactory() throws Exception {
////    	MongoOperations mongoOps = new MongoTemplate(mongo, "databaseName");
//        MongoClient mongoClient = new MongoClient("localhost", 27017);
//        UserCredentials userCredentials = new UserCredentials("", "");
//        return new SimpleMongoDbFactory(mongoClient, "example", userCredentials);
//    }
// 
	@Bean
	public MongoTemplate mongoTemplate() throws Exception {
	    MongoClient mongoClient = new MongoClient("localhost");
	    MongoDbFactory mongoDbFactory = new SimpleMongoDbFactory(mongoClient, "restaurant");
	    return new MongoTemplate(mongoDbFactory);
	}
//    @Bean
//    public MongoTemplate mongoTemplate() throws Exception {
//        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
//        return mongoTemplate;
//    }
 
}