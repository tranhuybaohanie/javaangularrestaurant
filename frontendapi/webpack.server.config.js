const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
// require('babel-polyfill');
module.exports = {
  entry: { server: './server.ts' },
  resolve: { extensions: ['.js', '.ts','.css'] },
  target: 'node',
  mode: 'none',
  externals:
   [/node_modules/],
  // [nodeExternals({ whitelist: [ /^@agm/core/, /^hammerjs/ ] })],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
          test: require.resolve('zepto'),
          use: 'imports-loader?this=>window'
      }
  ],
    rules: [{ test: /\.ts$/, loader: 'ts-loader' }, 
//     {
//       use: 'style-loader',
//       test: /\.css$/,
//   },
//   {
//     use: 'babel-loader',
//     test: /\.js$/,
//     exclude: '/node_modules/'
// },  {
//   use: 'css-loader',
//   test: /\.css$/,
// },
//   {
//     loader: 'file-loader',
//     test: /\.png$|\.gif$|\.jpe?g$|\.svg$|\.woff$|\.woff2$|\.eot$|\.ttf$|\.wav$|\.mp3$|\.mp4$|\.ico$/,
// }
]
  },
  plugins: [
    new webpack.DefinePlugin({
      window: undefined,
      document: undefined,
      location: JSON.stringify({
          protocol: 'https',
          host: `localhost`,
      })
  }),
    new webpack.ContextReplacementPlugin(
      /(.+)?angular(\\|\/)core(.+)?/,
      path.join(__dirname, 'src'),
      {}
    ),
    new webpack.ContextReplacementPlugin(
      /(.+)?express(\\|\/)(.+)?/,
      path.join(__dirname, 'src'),
      {}
    )
  ]
};