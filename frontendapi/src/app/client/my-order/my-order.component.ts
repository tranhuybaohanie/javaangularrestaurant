import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/admin/model/product.service';
import { OrderDaoService } from 'src/app/admin/services/order-dao.service';
import { Router } from '@angular/router';
import { OrderService } from 'src/app/admin/model/order.service';
import { LoginService } from 'src/app/admin/services/login.service';

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.scss']
})
  export class MyOrderComponent implements OnInit {
    itemList = [];
    numberItemOnePage = 5;
    pagedItems;
    deletedid;
    session = 0;
    appOrderDetail={
      order_id:0,
      show:false,
      cart:[]
    }
    constructor(public loginService:LoginService,public modelItem: ProductService, public itemDao: OrderDaoService, public router: Router) { }
    public notification = {
      title: "sadsflasdkjfl",
      notificationType: "success animated bounce",
      bodyText: "",
      show: false
    }
  
    setNotification(title, type, body, show) {
      this.notification.title = title;
      this.notification.notificationType = type;
      this.notification.bodyText = body;
      this.notification.show = show;
  
    }
    confirm = {
      title: "ok",
      content: "content",
      show: false
    }
    setConfirm(title, content, show) {
      this.confirm.title = title;
      this.confirm.content = content;
      this.confirm.show = show;
    }
    delete(id) {
  
      this.deletedid = id;
      this.setConfirm("Confirm", "Do you really want to delete " + id, true);
    }
    yes() {
      this.itemDao.deleteById(this.deletedid,
        () => {
          this.setNotification("Congratulation", "success animated bounce", "You have deleted a item successful: ", true);
        },
        () => {
          this.setNotification("Fail", "danger", "Fail to delete", true);
        })
      this.setConfirm("Confirm", "Do you really want to delete ", false);
    }
    no() {
      this.setConfirm("Confirm", "Do you really want to delete ", false);
    }
  
    ngOnInit() {
     
      this.itemDao.initializeWebSocketConnection(result => {
        this.itemList = [];
        JSON.parse(result).map((item, idd) => {
          var itemNew = new OrderService();
          itemNew.id = item.id;
          itemNew.code = item.code;
          itemNew.user = item.user;
          itemNew.phone = item.phone;
          itemNew.address = item.address;
          itemNew.create_date = item.createDate;
          itemNew.deliveringState = item.deliveringState;
          itemNew.receivedState = item.receivedState;
          itemNew.deliveredState = item.deliveredState;
          itemNew.fullName = item.fullName;
          itemNew.status = item.status;
          this.itemList.push(itemNew)
          this.session = this.session + 1;
        })
      })
      this.loginService.isAuthEmail(re=>{
       
      this.itemDao.getByUserEmail(re.email,result => {
        result.map((item, idd) => {
          var itemNew = new OrderService();
          itemNew.id = item.id;
          itemNew.code = item.code;
          itemNew.user = item.user;
          itemNew.phone = item.phone;
          itemNew.address = item.address;
          itemNew.create_date = item.createDate;
          itemNew.deliveringState = item.deliveringState;
          itemNew.receivedState = item.receivedState;
          itemNew.deliveredState = item.deliveredState;
          itemNew.fullName = item.fullName;
          itemNew.status = item.status;
          this.itemList.push(itemNew)
          this.session = this.session + 1;
        })
      }, err => {
        console.log(err)
      })
    },er=>{})
    }
   
    updateItems(id) {
      this.router.navigate(['/admin/product/update'], {
        queryParams: {
          id: id
        }
      });
    }
    showOrderDetail(id){
      this.appOrderDetail.order_id=id;
     this.appOrderDetail.show=true;
     this.itemDao.getOrderItemListByOrderId(id, re => {
      //this.cartSession=parseInt(localStorage.getItem("cartSession"))
      console.log(re)
      this.appOrderDetail.cart=re
    }, err => {
      alert(err);
    })


    }
    changeReceived(id){
      var itemNew = new OrderService();
      itemNew.id = id;
     
      itemNew.deliveringState = false;
      itemNew.receivedState = true;
      itemNew.deliveredState = false;
      itemNew.status = true;
      this.itemDao.update(itemNew,1,
        success => {
          this.setNotification("Congratulation", "success animated bounce", "You have check a item successful : " + id, true);
        },
        err => {
          this.setNotification("Fail", "danger", "Fail ", true);
        
      })
    }
    changeDelivering(id){
      var itemNew = new OrderService();
      itemNew.id = id;
     
      itemNew.deliveringState = true;
      itemNew.receivedState = true;
      itemNew.deliveredState = false;
      itemNew.status = true;
      this.itemDao.update(itemNew,2,
        success => {
          this.setNotification("Congratulation", "success animated bounce", "You have check a item successful : " + id, true);
        },
        err => {
          this.setNotification("Fail", "danger", "Fail ", true);
        
      })
    }
    changeDelivered(id){
      var itemNew = new OrderService();
      itemNew.id = id;
     
      itemNew.deliveringState = true;
      itemNew.receivedState = true;
      itemNew.deliveredState = true;
      itemNew.status = true;
      this.itemDao.update(itemNew,3,
        success => {
          this.setNotification("Congratulation", "success animated bounce", "You have check a item successful : " + id, true);
        },
        err => {
          this.setNotification("Fail", "danger", "Fail ", true);
        
      })
    }
    changeStatus(id){
      var itemNew = new OrderService();
      itemNew.id = id;
     
      itemNew.deliveringState = false;
      itemNew.receivedState = false;
      itemNew.deliveredState = false;
      itemNew.status = false;
      this.itemDao.update(itemNew,4,
        success => {
          this.setNotification("Congratulation", "success animated bounce", "You have check a item successful : " + id, true);
        },
        err => {
          this.setNotification("Fail", "danger", "Fail ", true);
        
      })
          }
  
  }
  

