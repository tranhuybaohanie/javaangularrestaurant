import { Component, OnInit } from '@angular/core';
import { ProductdaoService } from 'src/app/admin/services/productdao.service';
import { ProductService } from 'src/app/admin/model/product.service';
import { BigcategoryService } from 'src/app/admin/model/bigcategory.service';
import { BigcategorydaoService } from 'src/app/admin/services/bigcategorydao.service';
import { ProductcategorydaoService } from 'src/app/admin/services/productcategorydao.service';
import { ProductcategoryService } from 'src/app/admin/model/productcategory.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss']
})
export class ProductFilterComponent implements OnInit {
  itemList = [];
  numberItemOnePage = 12;
  pagedItems = [];
  session = 0;
  category = [];
  category_title = "";
  category_description = "";
  id;
  constructor(public activatedRoute: ActivatedRoute, public product: ProductService, public productDao: ProductdaoService, public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService, public categoryService: ProductcategoryService, public categorydao: ProductcategorydaoService) {

    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['spm'];
      this.productDao.getByCategoryId(this.id, result => {

        this.itemList = result;


      }, err => {

      })
    });

  }

  ngOnInit() { }

  id_new = ""
  ngDoCheck() {
    if (this.id != this.id_new) {
      this.id_new = this.id

      this.categorydao.getAll(result => {
        console.log(result.filter(x => x.id == this.id))
        this.category_title = result.filter(x => x.id == this.id)[0].nameVi
        this.category_description = result.filter(x => x.id == this.id)[0].descriptionVi;

      }, er => {

      })
      this.productDao.getByCategoryId(this.id, result => {

        this.itemList = result;


      }, err => {

      })
    }
  }


}
