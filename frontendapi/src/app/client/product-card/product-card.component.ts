import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input() public listProduct=[];
  @Input() public class="";
  cardText;
  constructor(public router:Router) { }

  ngOnInit() {
  }
  ngOnChanges() {
    console.log(this.listProduct)
  }
  redirectProductDetailById(id){
   
    this.router.navigate(['/product'], {
      queryParams: {
        id: id
      }
    });
  }
  showDetail(){
    this.cardText="card-text animated swing";
  }
  hideDetail(){
    this.cardText="card-text hide";
  }
  round(money){
    return Math.round(money); 
  }
}
