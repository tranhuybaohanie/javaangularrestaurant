import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-newmember',
  templateUrl: './newmember.component.html',
  styleUrls: ['./newmember.component.scss']
})
export class NewmemberComponent implements OnInit {
  fullname;
  constructor( public activatedRoute: ActivatedRoute) {

    this.activatedRoute.queryParams.subscribe(params => {
      this.fullname = params['fullname'];
    });
  }
  ngOnInit() {
  }

}
