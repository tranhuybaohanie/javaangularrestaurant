import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/admin/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email:string="";
  password:string="";
  success;
  error;
  urlReturn="";
  constructor(public loginService:LoginService,public router:Router) { }
login(){
  this.loginService.login(this.email,this.password,result=>{
  
    this.success="Congratulation! wellcome to koreco dashboard."
    this.error="";
    location.href="/";
    // if(this.urlReturn=""){
    //   this.router.navigate(["/home"]);
    // }else{
    //   this.router.navigate([this.urlReturn]);
    // }
   
   },
   err=>{
  if(err){
    this.success="";
    this.error=err;
  }
   
  
 },
   
   )
}
  ngOnInit() {
  }

}
