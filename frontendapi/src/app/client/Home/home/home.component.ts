import { Component, OnInit } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HostListener } from '@angular/core';
import { ProductdaoService } from 'src/app/admin/services/productdao.service';
import { ProductService } from 'src/app/admin/model/product.service';
import { BigcategoryService } from 'src/app/admin/model/bigcategory.service';
import { BigcategorydaoService } from 'src/app/admin/services/bigcategorydao.service';
import { ProductcategorydaoService } from 'src/app/admin/services/productcategorydao.service';
import { ProductcategoryService } from 'src/app/admin/model/productcategory.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss',
   "../../../../assets/vendor/bootstrap4/css/bootstrap.min.css",
  "../../../../assets/css/3-col-portfolio.css"],
  
})
export class HomeComponent implements OnInit {
  boxStar="box-star";
  cardText="card-text hide";
  index = 0;
  infinite = true;
  direction = 'right';
  directionToggle = true;
  autoplay = true;
  listProduct=[];
  listProductDiscount=[];
  listProductNewest=[];
  avatars = '1234567890'.split('').map((x, i) => {
    const num = i;
    // const num = Math.floor(Math.random() * 1000);
    return {
      url: `https://picsum.photos/600/400/?${num}`,
      title: `${num}`
    };
  });
  constructor(public router: Router,public product:ProductService, public productDao:ProductdaoService,public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService,public category: ProductcategoryService, public categorydao: ProductcategorydaoService) {
  
   }
 
  @HostListener("window:scroll", []) onWindowScroll() {
    // do some stuff here when the window is scrolled
    const verticalOffset = window.pageYOffset 
          || document.documentElement.scrollTop 
          || document.body.scrollTop || 0;
 
          if( document.getElementById("discount").offsetTop<verticalOffset+100)
         {
         this.boxStar="box-star animated jackInTheBox";
        }
        
}
  indexChanged(index) {
    console.log(index);
  }
  redirectProductDetailById(id){
   
    this.router.navigate(['/product'], {
      queryParams: {
        id: id
      }
    });
  }
  showDetail(){
    this.cardText="card-text animated swing";
  }
  hideDetail(){
    this.cardText="card-text hide";
  }
  round(money){
    return Math.round(money); 
  }
  ngOnInit() {

    this.productDao.getAll(result=>{
     
      this.listProduct=result;
      this.listProductDiscount=result.sort((a,b)=>{return b.promotionPrice/b.price-a.promotionPrice/a.price}).filter((item,id)=>{return id<6?item:null})
      this.listProductNewest=result.sort(function(a, b){
        var objectDateTimea=generateDateTime(a.createDate);
        var objectDateTimeb=generateDateTime(b.createDate);
        var a1 = new Date(objectDateTimea.Year+"/"+objectDateTimea.Month+"/"+objectDateTimea.Day+","+objectDateTimea.Hour+":"+objectDateTimea.Minute+":"+objectDateTimea.Second);
        var a2 = new Date(objectDateTimeb.Year+"/"+objectDateTimeb.Month+"/"+objectDateTimeb.Day+ ","+objectDateTimeb.Hour+":"+objectDateTimeb.Minute+":"+objectDateTimeb.Second);
        return (a2.getTime()-a1.getTime());
        }).filter((item,id)=>{return id<8?item:null})
    this.listProductNewest.filter(item=>console.log(item.createDate))
      //sort newest
      function generateDateTime(dateTimeChain){
        var Date=dateTimeChain.split(" ")[0];
        var Time=dateTimeChain.split(" ")[1];
        
        var Year=Date.split("/")[2];
         var Month=Date.split("/")[1];
         var Day=Date.split("/")[0];
         
         var Second=Time.split(":")[2];
         var Minute=Time.split(":")[1];
         var Hour=Time.split(":")[0];
         return {Year,Month,Day,Second,Minute,Hour};
         
        }
        
        
     
    },err=>{

    })
  }

}
