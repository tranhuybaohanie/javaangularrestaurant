import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';
import { OrderDaoService } from 'src/app/admin/services/order-dao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  @Input() public show: boolean = true;
  @Output() changeDisable = new EventEmitter<boolean>();
  customerInfo={
    fullName:"",
    phone:"",
    address:""
  }
  err="";
  quantity;
  totalMoney;
  cart=[];
  cartSession=0;
    constructor(public orderDao:OrderDaoService, public router: Router) { }
  
    ngOnInit() {
      if(localStorage.getItem("cartSession")!=null){
        this.cartSession=parseInt(localStorage.getItem("cartSession"))
     this.cart=   JSON.parse(localStorage.getItem("cart"))
      }  
    }
    btnClose(){
      this.changeDisable.emit(false)
    }
    ngDoCheck(){
     
      if(localStorage.getItem("cartSession")!=null&&parseInt(localStorage.getItem("cartSession"))!=this.cartSession){
        this.cartSession=parseInt(localStorage.getItem("cartSession"))
     this.cart=   JSON.parse(localStorage.getItem("cart"))
      }  
       this.quantity=0;
       this.totalMoney=0;
      JSON.parse(localStorage.getItem("cart")).map(item=>{
        this.quantity+=parseInt(item.quantity);
        this.totalMoney+=(item.promotion_price>0?item.promotion_price:item.price)*item.quantity;
      })
     
    }
    deleteItem(id){
      
      var cart=[]
      if(localStorage.getItem("cart")!=null){
       cart=JSON.parse(localStorage.getItem("cart"));
       var inclu= cart.filter(x=>x.id==id);
      if(inclu.length>0){
        var newCart=cart.filter(x=>x.id!=id);
        localStorage.setItem("cartSession",parseInt(localStorage.getItem("cartSession"))?"0":(parseInt(localStorage.getItem("cartSession"))+1).toString());
      localStorage.setItem("cart",JSON.stringify(newCart));
    
      }
      }
     
    }
  fixQuantity(id, quantityN){
    var cart=[]
    if(localStorage.getItem("cart")!=null){
     cart=JSON.parse(localStorage.getItem("cart"));
     var inclu= cart.filter(x=>x.id==id);
    if(inclu.length>0){
      var itemIncart=cart.filter(x=>x.id==id)[0];
      var newCart=cart.filter(x=>x.id!=id);
      itemIncart.quantity=quantityN;
      newCart.push(itemIncart)
  
    }
    }
    localStorage.setItem("cartSession",parseInt(localStorage.getItem("cartSession"))?"0":(parseInt(localStorage.getItem("cartSession"))+1).toString());
    localStorage.setItem("cart",JSON.stringify(cart));
  }
  
    priceFormat(money){
      var invertMoney="";
     
      var id=1;
       money.toString().split("").reverse().map((item,idd)=>{
         invertMoney+=item;
         if(id>1&&money.toString().split("").length&&id%3==0){
           invertMoney+="."
         }
         id++;
        
       })
       return invertMoney.toString().split("").reverse().join("");
    }
    btnPlaceOrder(e){
      e.preventDefault();
      if(this.customerInfo.address==""||this.customerInfo.phone==""||this.customerInfo.fullName==""){
        this.err="Please fullfill all abow infomation, we can not deliver foods without these info!"
      }else{
this.orderDao.addnew(this.customerInfo,re=>{
  localStorage.setItem("cartSession","0");
  localStorage.setItem("cart",JSON.stringify([]));
  this.router.navigate(['/success-checkout'], {
    // queryParams: {
    //   id: ids
    // }
  });
},er=>{
  alert("Sorry, there are some error occured!")
})
      }
    }
  

}
