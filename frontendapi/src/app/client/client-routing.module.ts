import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Home/home/home.component';
import { ClientComponent } from './client.component';
// import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AboutComponent } from './About/about/about.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductComponent } from './product/product/product.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { RegisterComponent } from './register/register.component';
import { NewmemberComponent } from './newmember/newmember.component';
import { AuthLoginGuard } from './auth-login.guard';
import { SuccessCheckoutComponent } from './success-checkout/success-checkout.component';
import { ContactComponent } from './contact/contact.component';
import { AddressComponent } from './address/address.component';
import { ProductFilterComponent } from './product-filter/product-filter.component';
import { MyOrderComponent } from './my-order/my-order.component';


const routes: Routes = [

  // { path: '', component: HomeComponent,redirectTo:"/home"},
  // { path: '**', redirectTo: '/home' },
  { path: '', component: ClientComponent,
children:[
  { path: '',  pathMatch: 'full',component:HomeComponent},
  {path:'home',component:HomeComponent},
  {path:'product-list',component:ProductListComponent},
  {path:'about',component:AboutComponent},
  {path:'login',component:LoginComponent},
  {path:'product',component:ProductComponent},
  {path:'checkout',component:CheckoutComponent, canActivate:[AuthLoginGuard]},
  {path:'register',component:RegisterComponent},
  {path:'newmember',component:NewmemberComponent},
  {path:'success-checkout',component:SuccessCheckoutComponent},
  {path:'contact',component:ContactComponent},
  {path:'address',component:AddressComponent},
  {path:'product-filter/:id',component:ProductFilterComponent},
  {path:'my-order',component:MyOrderComponent}
]
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
