import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import '../../assets/js/test.js'
import { Router } from '@angular/router';
import { ProductdaoService } from 'src/app/admin/services/productdao.service';
import { ProductService } from 'src/app/admin/model/product.service';
import { BigcategoryService } from 'src/app/admin/model/bigcategory.service';
import { BigcategorydaoService } from 'src/app/admin/services/bigcategorydao.service';
import { ProductcategorydaoService } from 'src/app/admin/services/productcategorydao.service';
import { ProductcategoryService } from 'src/app/admin/model/productcategory.service';
import { LoginService } from '../admin/services/login.service.js';
import { StringFormatService } from '../Utilities/string-format.service.js';
// import "jquery";
// import 'popper.js'
// import "../../assets/js/bootstrap.min.js" ;
// declare var variableName:any;

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  show: string = "";
  cart = { show: false }
  category = [];
  bigCategory = [];
  quantity=0;
  authName="";

  constructor(public StringFormat:StringFormatService,public loginService:LoginService,private router: Router, public product: ProductService, public productDao: ProductdaoService, public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService, public categoryModel: ProductcategoryService, public categorydao: ProductcategorydaoService) { }

  onScroll(e) {

  }
  btnShowCart() {
    this.cart.show = true;
  }
  logout(){
    this.loginService.logout(re=>{this.authName=""},er=>{})
  }
  ngDoCheck() {
  
    if (this.show != this.router.url) {
      this.show = this.router.url;
      
    }
    if( localStorage.getItem("cart")!=null&&typeof  JSON.parse(localStorage.getItem("cart"))=="object"){
    this.quantity=0;
   // console.log(localStorage.getItem("cart"));
    JSON.parse(localStorage.getItem("cart")).map(item=>{
      this.quantity+=parseInt(item.quantity);
    })
  }
  //else    if( localStorage.getItem("cart")==null) this.quantity=0;
  }
  getCategoryByBigCateId(item,cate) {
    return cate.filter(x => x.bigCategoryId == item.id);

  }
  navigateProductFilter(e) {
   
    this.router.navigate(['/product-filter/'+this.StringFormat.cleanAccents(e.nameVi).split(" ").join("-") +"/"], {
    
      queryParams: {
        spm: e.id
      }
    });
  }
  ngOnInit() {
    this.loginService.isAuthName(re=>{this.authName=re.fullname},er=>{})
    this.bigcategorydao.getAll(result => {

      this.bigCategory = result;
    }, er => {

    })
    this.categorydao.getAll(result => {


      this.category= result;
    
    }, er => {

    })
  
    var parallax = document.querySelectorAll("body"),
      speed = 0.7;

    window.onscroll = function () {
      [].slice.call(parallax).forEach(function (el, i) {

        var windowYOffset = window.pageYOffset,
          elBackgrounPos = "50% " + (windowYOffset * speed) + "px";

        el.style.backgroundPosition = elBackgrounPos;

      });
    };
  }

}
