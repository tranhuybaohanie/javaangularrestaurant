import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-success-checkout',
  templateUrl: './success-checkout.component.html',
  styleUrls: ['./success-checkout.component.scss']
})
export class SuccessCheckoutComponent implements OnInit {

  fullname;
  constructor( public activatedRoute: ActivatedRoute) {

    this.activatedRoute.queryParams.subscribe(params => {
      this.fullname = params['fullname'];
    });
  }
  ngOnInit() {
  }

}
