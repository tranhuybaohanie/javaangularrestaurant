import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/admin/model/product.service';
import { ProductdaoService } from 'src/app/admin/services/productdao.service';
import { ProductcategorydaoService } from 'src/app/admin/services/productcategorydao.service';
import { BigcategoryService } from 'src/app/admin/model/bigcategory.service';
import { BigcategorydaoService } from 'src/app/admin/services/bigcategorydao.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  session;
  bigcategoryList;
  productcategoryList;
  uploadImageFor;// profile,option
  updateId;
  itemImgMain="";
  listProductNewest=[];
  quantity=1;
  public notification = {
    title: "sadsflasdkjfl",
    notificationType: "success animated bounce",
    bodyText: "",
    show: false
  }
  numberItemOnePage = 1;
  public imgLibrary = {
    returnUrl: "",
    show: false
  }
  setNotification(title, type, body, show) {
    this.notification.title = title;
    this.notification.notificationType = type;
    this.notification.bodyText = body;
    this.notification.show = show;
  }
  addCart(){
  // localStorage.setItem("cart",JSON.stringify([]));
    var cart=[]
    if(localStorage.getItem("cart")!=null){
     cart=JSON.parse(localStorage.getItem("cart"));
     var inclu= cart.filter(x=>x.id==this.itemModel.id);
    if(inclu.length>0){
      var itemIncart=cart.filter(x=>x.id==this.itemModel.id)[0];
      var newCart=cart.filter(x=>x.id!=this.itemModel.id);
      itemIncart.quantity=parseInt(itemIncart.quantity)+this.quantity;
      newCart.push(itemIncart)

    }else{
      cart.push({...this.itemModel,quantity:this.quantity});
     
    }
    }else{
      cart.push({...this.itemModel,quantity:this.quantity});
     
    }
    localStorage.setItem("cartSession",parseInt(localStorage.getItem("cartSession"))?"0":(parseInt(localStorage.getItem("cartSession"))+1).toString());
    localStorage.setItem("cart",JSON.stringify(cart));
    
  }
  showImgLibrary(uploadImageFor) {
    if (uploadImageFor == "profile") {
      this.uploadImageFor = "profile";
    } else if (uploadImageFor == "option") {
      this.uploadImageFor = "option";
    }
    this.imgLibrary.show = true
  }
  constructor(public itemModel: ProductService, public itemDao: ProductdaoService, public productCategoryDao: ProductcategorydaoService, public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService, public activatedRoute: ActivatedRoute) {
    this.itemModel.status = true;

    this.itemModel.status = true;
    this.activatedRoute.queryParams.subscribe(params => {
      this.updateId = params['id'];
    });
  }
  changeStatus() {
    this.itemModel.status = !this.itemModel.status;
  }
  convertStringToArray(item: string, splitValue: string) {
    if (item.length > 0)
      return item.split(splitValue);
  }
  getUrlLibrary(url) {
    if (this.uploadImageFor == "profile") {
      this.itemModel.img_url = url;
    } else if (this.uploadImageFor == "option") {
      if (typeof this.itemModel.img_more_url == "undefined") {
        this.itemModel.img_more_url = url;

      } else {
        this.itemModel.img_more_url = this.itemModel.img_more_url + "{/breakbao$#^imgmore/}" + url;

      }

    }


    this.imgLibrary.show = false;
  }

  deleteImgMore(e) {
    var imgSTring = e.target.src;
    this.itemImgMain=imgSTring;
    // var imgAr = this.itemModel.img_more_url.split("{/breakbao$#^imgmore/}");

    // this.itemModel.img_more_url = "";
    // imgAr.map((item, id) => {
    //   if (item != imgSTring) {
    //     if (id == 0) {
    //       this.itemModel.img_more_url = item;
    //     } else {
    //       this.itemModel.img_more_url = this.itemModel.img_more_url + "{/breakbao$#^imgmore/}" + item;
    //     }

    //   }

    // })
  }
  submitForm() {

    this.itemDao.update(this.itemModel,
      success => {
        this.setNotification("Congratulation", "success animated bounce", "You have added a item successful: " + this.itemModel.name, true);
      },
      err => {
        this.setNotification("Fail", "danger", "Fail to add: " + this.itemModel.name, true);
      }
    );

  }
  getBigCategoryById(itemModel,bigcategoryList){
    if(typeof itemModel.big_category_id !="undefined" && typeof bigcategoryList == "object"  && bigcategoryList.length>0){
    return  bigcategoryList.filter(x=>x.id==itemModel.big_category_id)[0]}
  }

  getCategoryById(itemModel,productcategoryList){
    if(typeof itemModel.product_category_id !="undefined"&& typeof productcategoryList == "object" && productcategoryList.length>0){
   // console.log(productcategoryList.filter(x=>x.id==itemModel.product_category_id)[0])
      // this.productCategoryDao.getById(itemModel.product_category_id,r=>{return r},e=>{})
      return productcategoryList.filter(x=>x.id==itemModel.product_category_id)[0]
  }
  }
  round(money){
    return Math.round(money); 
  }
  async ngOnInit() {

    this.bigcategorydao.getAll(result => {
      this.bigcategoryList = [];
      result.map((item, idd) => {
        var bigcategory = new BigcategoryService();
        //console.log(item)
        bigcategory.id = item.id;
        bigcategory.name = item.nameVi;
        bigcategory.description = item.descriptionVi;
        bigcategory.author = item.author;
        bigcategory.create_date = item.createDate;
        bigcategory.img_url = item.imgUrl;
        bigcategory.description_seo = item.metaDiscription;
        bigcategory.focus_keyword = item.metaKeyword;
        bigcategory.meta_title = item.metaTitle;
        bigcategory.status = item.status;
        this.bigcategoryList.push(bigcategory)
        this.session = this.session + 1;
      })
    }, err => {
      console.log(err)
    })
    this.productCategoryDao.getAll(result => {
      this.productcategoryList = [];
      result.map((item, idd) => {
        var productcategory = new BigcategoryService();
        //console.log(item)
        productcategory.id = item.id;
        productcategory.name = item.nameVi;
        productcategory.description = item.descriptionVi;
        productcategory.author = item.author;
        productcategory.create_date = item.createDate;
        productcategory.img_url = item.imgUrl;
        productcategory.description_seo = item.metaDiscription;
        productcategory.focus_keyword = item.metaKeyword;
        productcategory.meta_title = item.metaTitle;
        productcategory.status = item.status;
        this.productcategoryList.push(productcategory)
        this.session = this.session + 1;
      })
    }, err => {
      console.log(err)
    })
    this.itemDao.getById(this.updateId, success => {
      //console.log(success)
      this.itemModel = success;
      var e = {target:{value:this.itemModel.big_category_id}}
      this.loadProductCategory(e) ;
      this.itemDao.getAll(result=>{
        //console.log(result)
      
        this.listProductNewest=result.sort(function(a, b){
          var objectDateTimea=generateDateTime(a.createDate);
          var objectDateTimeb=generateDateTime(b.createDate);
          var a1 = new Date(objectDateTimea.Year+"/"+objectDateTimea.Month+"/"+objectDateTimea.Day+","+objectDateTimea.Hour+":"+objectDateTimea.Minute+":"+objectDateTimea.Second);
          var a2 = new Date(objectDateTimeb.Year+"/"+objectDateTimeb.Month+"/"+objectDateTimeb.Day+ ","+objectDateTimeb.Hour+":"+objectDateTimeb.Minute+":"+objectDateTimeb.Second);
          return  (a2.getTime()-a1.getTime());;
          }).filter((item,id)=>{return id<8?item:null})
      this.listProductNewest.filter(item=>console.log(item.createDate))
        //sort newest
        function generateDateTime(dateTimeChain){
          var Date=dateTimeChain.split(" ")[0];
          var Time=dateTimeChain.split(" ")[1];
          
          var Year=Date.split("/")[2];
           var Month=Date.split("/")[1];
           var Day=Date.split("/")[0];
           
           var Second=Time.split(":")[2];
           var Minute=Time.split(":")[1];
           var Hour=Time.split(":")[0];
           return {Year,Month,Day,Second,Minute,Hour};
           
          }
          
          
       
      },err=>{
  
      })
    },
      err => {
        this.setNotification("Fail", "danger", "Fail to load", true);
      }
    )
  }

  loadProductCategory(e) {
    this.productCategoryDao.getAllByBigCategoryId(e.target.value, result => {
    //  console.log(result)
      this.productcategoryList = result;
    }, err => {
      this.setNotification("Load Fsail", "danger", "Fail to load product category, please reload this page!", true);
    })

  }

}