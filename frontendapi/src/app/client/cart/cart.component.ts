import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
@Input() public show: boolean = true;
@Output() changeDisable = new EventEmitter<boolean>();
quantity;
totalMoney;
cart=[];
cartSession=0;
  constructor() { }

  ngOnInit() {
    if(localStorage.getItem("cartSession")!=null){
      this.cartSession=parseInt(localStorage.getItem("cartSession"))
   this.cart=   JSON.parse(localStorage.getItem("cart"))
    }  
  }
  btnClose(){
    this.changeDisable.emit(false)
  }
  ngDoCheck(){
   
    if(localStorage.getItem("cartSession")!=null&&parseInt(localStorage.getItem("cartSession"))!=this.cartSession){
      this.cartSession=parseInt(localStorage.getItem("cartSession"))
   this.cart=   JSON.parse(localStorage.getItem("cart"))
    }  
    if( localStorage.getItem("cart")!=null&&typeof  JSON.parse(localStorage.getItem("cart"))=="object"){
     this.quantity=0;
     this.totalMoney=0;

    JSON.parse(localStorage.getItem("cart")).map(item=>{
      this.quantity+=parseInt(item.quantity);
      this.totalMoney+=(item.promotion_price>0?item.promotion_price:item.price)*item.quantity;
    })
  }
   
  }
  deleteItem(id){
    
    var cart=[]
    if(localStorage.getItem("cart")!=null){
     cart=JSON.parse(localStorage.getItem("cart"));
     var inclu= cart.filter(x=>x.id==id);
    if(inclu.length>0){
      var newCart=cart.filter(x=>x.id!=id);
      localStorage.setItem("cartSession",parseInt(localStorage.getItem("cartSession"))?"0":(parseInt(localStorage.getItem("cartSession"))+1).toString());
    localStorage.setItem("cart",JSON.stringify(newCart));
  
    }
    }
   
  }
fixQuantity(id, quantityN){
  var cart=[]
  if(localStorage.getItem("cart")!=null){
   cart=JSON.parse(localStorage.getItem("cart"));
   var inclu= cart.filter(x=>x.id==id);
  if(inclu.length>0){
    var itemIncart=cart.filter(x=>x.id==id)[0];
    var newCart=cart.filter(x=>x.id!=id);
    itemIncart.quantity=quantityN;
    newCart.push(itemIncart)

  }
  }
  localStorage.setItem("cartSession",parseInt(localStorage.getItem("cartSession"))?"0":(parseInt(localStorage.getItem("cartSession"))+1).toString());
  localStorage.setItem("cart",JSON.stringify(cart));
}

  priceFormat(money){
    var invertMoney="";
   
    var id=1;
     money.toString().split("").reverse().map((item,idd)=>{
       invertMoney+=item;
       if(id>1&&money.toString().split("").length&&id%3==0){
         invertMoney+="."
       }
       id++;
      
     })
     return invertMoney.toString().split("").reverse().join("");
  }

}
