import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  lat: number =  10.810583;
  lng: number =  106.709145;

  constructor() { }

  ngOnInit() {
  }

}
