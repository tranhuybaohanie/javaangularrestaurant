import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/admin/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss',
]
})
export class RegisterComponent implements OnInit {
email:string="";
password:string="";
password2:string="";
fullname:string="";
password_err="";
password_err2="";
email_err="";
err="";
  constructor(public login:LoginService,public router:Router) { }
  signup(){
    if(this.email==""||!this.email.includes("@")||!this.email.includes(".")){
      this.email_err="This email is not valid!"
    }else{
      this.email_err=""; 
      if(this.password.length<8){
        this.password_err="Password need longer than 8 character!"
      }else{
        this.password_err=""; 
        if(this.password!=this.password2){
          this.password_err2="Repeat password not coincide password!"
        }else{
          this.password_err2=""; 
          this.login.customerRegister(this.fullname,this.email,this.password,re=>{
            if(re==true){
              this.router.navigate(['/newmember'], {
                queryParams: {
                  fullname:this.fullname 
                }
              });
            }else{
              this.err=re;
            }
          },err=>{
            this.err=err;
          })
        }
      }
    }
   
  
  }
  ngOnInit() {
  }

}
