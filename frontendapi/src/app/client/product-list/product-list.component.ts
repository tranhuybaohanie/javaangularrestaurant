import { Component, OnInit } from '@angular/core';
import { ProductdaoService } from 'src/app/admin/services/productdao.service';
import { ProductService } from 'src/app/admin/model/product.service';
import { BigcategoryService } from 'src/app/admin/model/bigcategory.service';
import { BigcategorydaoService } from 'src/app/admin/services/bigcategorydao.service';
import { ProductcategorydaoService } from 'src/app/admin/services/productcategorydao.service';
import { ProductcategoryService } from 'src/app/admin/model/productcategory.service';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  itemList = [];
  numberItemOnePage = 12;
  pagedItems;
  session = 0;
  category=[];
  constructor(public product:ProductService, public productDao:ProductdaoService,public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService,public categoryService: ProductcategoryService, public categorydao: ProductcategorydaoService) { }

  ngOnInit() {
    this.categorydao.getAll(result=>{
      console.log(result)
      this.category=result;
    },er=>{
      
    })
    this.productDao.getAll(result=>{
      // console.log(result)
      this.itemList=result;
     
     
    },err=>{

    })
  }
  ngOnChange(){
    this.categorydao.getAll(result=>{
      console.log(result)
      this.category=result;
    },er=>{
      
    })
    this.productDao.getAll(result=>{
      // console.log(result)
      this.itemList=result;
     
     
    },err=>{

    })
  }
  

}
