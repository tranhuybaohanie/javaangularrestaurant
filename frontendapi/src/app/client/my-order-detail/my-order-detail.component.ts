import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OrderDaoService } from 'src/app/admin/services/order-dao.service';

@Component({
  selector: 'app-my-order-detail',
  templateUrl: './my-order-detail.component.html',
  styleUrls: ['./my-order-detail.component.scss']
})
export class MyOrderDetailComponent implements OnInit {
  @Input() public show: boolean = true;
  @Input() public order_id: string="1";
  @Output() changeDisable = new EventEmitter<boolean>();
  quantity=0;
  totalMoney=0;
  @Input() public cart :Array<any>;
  cartSession = 1;
  constructor(public orderItemDao: OrderDaoService) { }

  ngOnInit() {
    

  }
  ngOnChanges(){
    this.quantity = 0;
        this.totalMoney = 0;
   this.cart.map(item=>{
    console.log("okkokoko") 
    console.log(item)
    this.quantity += parseInt(item.quantity);
    this.totalMoney += (item.promotion_price > 0 ? item.promotion_price : item.price) * item.quantity;
   })
  }
  btnClose() {
    this.changeDisable.emit(false)
  }
  // ngDoCheck() {

  //   if (localStorage.getItem("cartSession") != null && parseInt(localStorage.getItem("cartSession")) != this.cartSession) {
  //     this.cartSession = parseInt(localStorage.getItem("cartSession"))
  //     this.cart = JSON.parse(localStorage.getItem("cart"))
  //   }
  //   if (localStorage.getItem("cart") != null && typeof JSON.parse(localStorage.getItem("cart")) == "object") {
  //     this.quantity = 0;
  //     this.totalMoney = 0;

  //     JSON.parse(localStorage.getItem("cart")).map(item => {
  //       console.log(item)
  //       this.quantity += parseInt(item.quantity);
  //       this.totalMoney += (item.promotion_price > 0 ? item.promotion_price : item.price) * item.quantity;
  //     })
  //   }

  // }
  deleteItem(id) {

    var cart = []
    if (localStorage.getItem("cart") != null) {
      cart = JSON.parse(localStorage.getItem("cart"));
      var inclu = cart.filter(x => x.id == id);
      if (inclu.length > 0) {
        var newCart = cart.filter(x => x.id != id);
        localStorage.setItem("cartSession", parseInt(localStorage.getItem("cartSession")) ? "0" : (parseInt(localStorage.getItem("cartSession")) + 1).toString());
        localStorage.setItem("cart", JSON.stringify(newCart));

      }
    }

  }
  fixQuantity(id, quantityN) {
    var cart = []
    if (localStorage.getItem("cart") != null) {
      cart = JSON.parse(localStorage.getItem("cart"));
      var inclu = cart.filter(x => x.id == id);
      if (inclu.length > 0) {
        var itemIncart = cart.filter(x => x.id == id)[0];
        var newCart = cart.filter(x => x.id != id);
        itemIncart.quantity = quantityN;
        newCart.push(itemIncart)

      }
    }
    localStorage.setItem("cartSession", parseInt(localStorage.getItem("cartSession")) ? "0" : (parseInt(localStorage.getItem("cartSession")) + 1).toString());
    localStorage.setItem("cart", JSON.stringify(cart));
  }

  priceFormat(money) {
    var invertMoney = "";

    var id = 1;
    money.toString().split("").reverse().map((item, idd) => {
      invertMoney += item;
      if (id > 1 && money.toString().split("").length && id % 3 == 0) {
        invertMoney += "."
      }
      id++;

    })
    return invertMoney.toString().split("").reverse().join("");
  }

}
