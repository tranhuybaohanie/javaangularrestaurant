import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { HomeComponent } from './Home/home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import 'hammerjs';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AboutComponent } from './About/about/about.component';
import { CartComponent } from './cart/cart.component';
import { ProductListComponent } from './product-list/product-list.component';
import { PaginationComponent } from './shared/pagination/pagination.component';
import { ProductComponent } from './product/product/product.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { NewmemberComponent } from './newmember/newmember.component';
import { SuccessCheckoutComponent } from './success-checkout/success-checkout.component';
import { ContactComponent } from './contact/contact.component';

import { AgmCoreModule } from '@agm/core';
import { AddressComponent } from './address/address.component';
import { ProductFilterComponent } from './product-filter/product-filter.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { MyOrderComponent } from './my-order/my-order.component';
import { OrderDetailComponent } from '../admin/order-detail/order-detail.component';
import { MyOrderDetailComponent } from './my-order-detail/my-order-detail.component';
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
  ClientComponent,
  HomeComponent,
  LoginComponent,
  RegisterComponent,
  AboutComponent,
  CartComponent,
  ProductListComponent,
  PaginationComponent,
  ProductComponent,
  CheckoutComponent,
  NewmemberComponent,
  SuccessCheckoutComponent,
  ContactComponent,
  AddressComponent,
  ProductFilterComponent,
  ProductCardComponent,
  MyOrderComponent,
  MyOrderDetailComponent,
  
  
],
  imports: [
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    ClientRoutingModule,
    FormsModule,
    HttpClientModule,
    MDBBootstrapModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCdod3je-nQdbi95gcdQBjMkdRrCCXgVAY'
    })
  ],
  providers: [],
  bootstrap: [ClientComponent]
})

export class ClientModule {    }










