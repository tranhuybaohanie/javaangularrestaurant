import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
//   template: `
//   <div>
//     <h2>{{ 'HOME.TITLE' | translate }}</h2>
//     <label>
//       {{ 'HOME.SELECT' | translate }}
//       <select #langSelect (change)="translate.use(langSelect.value)">
//         <option *ngFor="let lang of translate.getLangs()" [value]="lang" [selected]="lang === translate.currentLang">{{ lang }}</option>
//       </select>
//     </label>
//   </div>
// `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = "Restaurant";
  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'vi']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|vi/) ? browserLang : 'en');
  }
}
