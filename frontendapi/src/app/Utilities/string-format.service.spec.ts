import { TestBed } from '@angular/core/testing';

import { StringFormatService } from './string-format.service';

describe('StringFormatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StringFormatService = TestBed.get(StringFormatService);
    expect(service).toBeTruthy();
  });
});
