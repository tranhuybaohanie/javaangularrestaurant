import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirm-form',
  templateUrl: './confirm-form.component.html',
  styleUrls: ['./confirm-form.component.scss']
})
export class ConfirmFormComponent implements OnInit {
  @Input() public title:string="Confirm";
  @Input() public content:string="Do you really want to do this task?";
  @Input() public show:boolean=true;
  
  @Output() yes = new EventEmitter<boolean>();
  @Output() no = new EventEmitter<boolean>();

  constructor() { }
yesCo(){

  this.yes.emit()
}
noCo(){
  
  this.no.emit()
}
  ngOnInit() {
  }

}
