import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {


  id:string;
  code:string;
  phone:string;
  address:number;
  fullName:number;
  user:string;
  create_date:string;
  status:boolean;
  receivedState:boolean;
  deliveringState:boolean;
  deliveredState:boolean;
  

}
