import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  id:string;
  name:string;
  description:string;
  big_category_id:number;
  product_category_id:number;
  author:string;
  create_date:string;
  status:boolean;
  focus_keyword:string;
  meta_title:string;
  description_seo:string;
  img_url:string;
  img_more_url:string;
  currency:string;
  view_count:Int16Array;
  ingredient:string;
  nutritional_value:string;
  price:number;
  promotion_price:number;

}
