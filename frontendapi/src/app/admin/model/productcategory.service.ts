import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductcategoryService {

    id:string;
    name:string;
    description:string;
    big_category_id:number;
    author:string;
    create_date:string;
    status:boolean;
    focus_keyword:string;
    meta_title:string;
    description_seo:string;
    img_url:string;
}
