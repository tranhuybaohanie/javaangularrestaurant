import { TestBed } from '@angular/core/testing';

import { BigcategoryService } from './bigcategory.service';

describe('BigcategoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BigcategoryService = TestBed.get(BigcategoryService);
    expect(service).toBeTruthy();
  });
});
