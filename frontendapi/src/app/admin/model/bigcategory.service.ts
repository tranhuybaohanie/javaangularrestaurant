import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BigcategoryService {
  id:string;
  name:string;
  description:string;
  author:string;
  create_date:string;
  status:boolean;
  focus_keyword:string;
  meta_title:string;
  description_seo:string;
  img_url:string;
  
  }

