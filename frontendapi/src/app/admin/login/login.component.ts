import { Component, OnInit } from '@angular/core';
import config from '../../config'
import { getLocaleDayNames } from '@angular/common';
import { LoginService } from '../services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss',
  "../../../assets/Login_V2/vendor/bootstrap/css/bootstrap.min.css",
  "../../../assets/Login_V2/vendor/bootstrap/css/bootstrap.min.css",
  "../../../assets/Login_V2/fonts/font-awesome-4.7.0/css/font-awesome.min.css",
  "../../../assets/Login_V2/fonts/iconic/css/material-design-iconic-font.min.css",
  "../../../assets/Login_V2/vendor/animate/animate.css",
  "../../../assets/Login_V2/vendor/css-hamburgers/hamburgers.min.css",
  "../../../assets/Login_V2/vendor/animsition/css/animsition.min.css",
  "../../../assets/Login_V2/vendor/select2/select2.min.css",
  "../../../assets/Login_V2/vendor/daterangepicker/daterangepicker.css",
  "../../../assets/Login_V2/css/util.css",
  "../../../assets/Login_V2/css/main.css"
]
})
export class LoginComponent implements OnInit {
  brand:String=config.brand_name;
  year:string =new Date().getFullYear().toString();
  email:string="bao1@gmail.com";
  password:string="abc";
  error:string="";
  success:string="";
  urlReturn:string;
  constructor(private loginService: LoginService,private router: Router,private activatedRoute: ActivatedRoute) { 
    this.activatedRoute.queryParams.subscribe(params => {
      this.urlReturn = params['return'];
  });
  }
 
  login(){
   this.loginService.login(this.email,this.password,result=>{
  
    this.success="Congratulation! wellcome to koreco dashboard."
    this.error="";
    this.router.navigate([this.urlReturn]);
   },
   err=>{
  if(err){
    this.success="";
    this.error=err;
  }
   
  
 },
   
   )
    
  }
  ngOnInit() {
  }

}
