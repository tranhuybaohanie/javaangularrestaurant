import { Component, OnInit, Input ,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
@Input() public bodyText:string="A items was added"
@Input() public title:string="Congratetu lation"
@Input() public notificationType:string="success";
@Input() public show:boolean=true;
@Output() isShowChange = new EventEmitter<boolean>();
close=()=>{
 
  this.isShowChange.emit(false)
}
  constructor() { }

  ngOnInit() {
  }

}
