import { Component, OnInit } from '@angular/core';
import { BigcategoryService } from '../../model/bigcategory.service';
import { ProductcategoryService } from '../../model/productcategory.service';
import { BigcategorydaoService } from '../../services/bigcategorydao.service';
import { ProductcategorydaoService } from '../../services/productcategorydao.service';
import { ProductdaoService } from '../../services/productdao.service';
import { ProductService } from '../../model/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss',
    // '../../../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
    '../../../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
    // '../../../../assets/admindashboard/assets/vendor/linearicons/style.css',
    // '../../../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
    // '../../../../assets/admindashboard/assets/css/main.css',
    // '../../../../assets/admindashboard/assets/css/demo.css'
  ]
})
export class ProductAddComponent implements OnInit {
  session;
  bigcategoryList;
  productcategoryList;
  uploadImageFor;// profile,option

  public notification = {
    title: "sadsflasdkjfl",
    notificationType: "success animated bounce",
    bodyText: "",
    show: false
  }
  numberItemOnePage = 1;
  public imgLibrary = {
    returnUrl: "",
    show: false
  }
  setNotification(title, type, body, show) {
    this.notification.title = title;
    this.notification.notificationType = type;
    this.notification.bodyText = body;
    this.notification.show = show;
  }
  showImgLibrary(uploadImageFor) {
    if(uploadImageFor=="profile"){
      this.uploadImageFor="profile";
    }else if(uploadImageFor=="option"){
      this.uploadImageFor="option";
    }
    this.imgLibrary.show = true
  }
  constructor(public itemModel: ProductService, public itemDao: ProductdaoService,public productCategoryDao:ProductcategorydaoService, public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService) {
    this.itemModel.status = true;

  }
  changeStatus() {
    this.itemModel.status = !this.itemModel.status;
  }
  convertStringToArray(item:string,splitValue:string){
    if(item.length>0)
return item.split(splitValue);
  }
  getUrlLibrary(url) {
    if(this.uploadImageFor=="profile"){
      this.itemModel.img_url = url;
    }else  if(this.uploadImageFor=="option"){
      if(typeof this.itemModel.img_more_url=="undefined"){
        this.itemModel.img_more_url =url;
      
      }else{
        this.itemModel.img_more_url =this.itemModel.img_more_url + "{/breakbao$#^imgmore/}"+url;
     
      }
     
    }
 
   
    this.imgLibrary.show = false;
  }

  deleteImgMore(e){
    var imgSTring = e.target.src;
    var imgAr=this.itemModel.img_more_url.split("{/breakbao$#^imgmore/}");

    this.itemModel.img_more_url="";
    imgAr.map((item,id)=>{
      if(item!=imgSTring){
        if(id==0){
          this.itemModel.img_more_url = item;
        }else{
          this.itemModel.img_more_url = this.itemModel.img_more_url + "{/breakbao$#^imgmore/}"+item;
        }
      
      }
     
    })
  }
  submitForm() {
    this.itemDao.addnew(this.itemModel,
      success => {
        this.setNotification("Congratulation", "success animated bounce", "You have added a item successful: " + this.itemModel.name, true);
      },
      err => {
        this.setNotification("Fail", "danger", "Fail to add: " + this.itemModel.name, true);
      }
    );

  }
  ngOnInit() {
    this.bigcategorydao.getAll(result => {
      this.bigcategoryList = [];
      result.map((item, idd) => {
        var bigcategory = new BigcategoryService();
        //console.log(item)

        bigcategory.id = item.id;
        bigcategory.name = item.nameVi;
        bigcategory.description = item.descriptionVi;
        bigcategory.author = item.author;
        bigcategory.create_date = item.createDate;
        bigcategory.img_url = item.imgUrl;
        bigcategory.description_seo = item.metaDiscription;
        bigcategory.focus_keyword = item.metaKeyword;
        bigcategory.meta_title = item.metaTitle;
        bigcategory.status = item.status;
        this.bigcategoryList.push(bigcategory)
        this.session = this.session + 1;
      })
    }, err => {
      console.log(err)
    })} 
    loadProductCategory(e){
    this.productCategoryDao.getAllByBigCategoryId(e.target.value,result=>{
      console.log(result)
this.productcategoryList=result;
    },err=>{
      this.setNotification("Load Fsail", "danger", "Fail to load product category, please reload this page!", true);
    })
   
  }

}


