import { Injectable } from '@angular/core';
import { BigcategoryService } from '../model/bigcategory.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import config from '../../config';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
import { ProductcategoryService } from '../model/productcategory.service';
@Injectable({
  providedIn: 'root'
})
export class ProductcategorydaoService {
  private serverUrl = config.urlApiSocket + '/socket'
  private title = 'WebSockets chat';
  private stompClient;
  private listImagesSnapshot;
  constructor(private http: HttpClient) {


  }
  initializeWebSocketConnection(result) {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/productcategory/snapshot", (message) => {
        if (message.body) {
          // $(".chat").append("<div class='message'>"+message.body+"</div>")

          result(message.body);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send("/app/update/productcategory", {}, message);
    // $('#input').val('');
  }

  addnew(item: ProductcategoryService, result, error) {
    console.log(item)
    this.http.post<any>(config.urlApi + "/product-category",
      {
        nameVi: item.name || "",
        descriptionVi: item.description || "",
        status: item.status,
        imgUrl: item.img_url || "",
        metaKeyword: item.focus_keyword || "",
        metaDiscription: item.description_seo || "",
        metaTitle: item.meta_title || "",
        bigCategoryId: item.big_category_id


      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {
          if (typeof res.result != "undefined" && res.result) {
            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          console.log(err)
          error(JSON.stringify(err.error.error));

        }
      );
  }

  update(item: ProductcategoryService, result, error) {

    this.http.put<any>(config.urlApi + "/product-category",
      {
        id: item.id,
        nameVi: item.name || "",
        descriptionVi: item.description || "",
        status: item.status,
        imgUrl: item.img_url || "",
        metaKeyword: item.focus_keyword || "",
        metaDiscription: item.description_seo || "",
        metaTitle: item.meta_title || "",
        bigCategoryId: item.big_category_id || ""

      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }
  getById(id, result, error) {

    this.http.get<any>(config.urlApi + "/product-category/" + id,
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {
          var item = new ProductcategoryService();
          item.id = res.id;
          item.name = res.nameVi;
          item.description = res.descriptionVi;
          item.author = res.author;
          item.create_date = res.createDate;
          item.img_url = res.imgUrl;
          item.description_seo = res.metaDiscription;
          item.focus_keyword = res.metaKeyword;
          item.meta_title = res.metaTitle;
          item.big_category_id = res.bigCategoryId;
          item.status = res.status;

          result(item)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }

  getAll(result, error) {

    this.http.get<any>(config.urlApi + "/product-category",
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          result(res)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }
  getAllByBigCategoryId(big_category_id,result, error) {

    this.http.get<any>(config.urlApi + "/product-category/by-big-category-id/"+big_category_id,
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          result(res)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }


  deleteById(id, success, error) {
    this.http.delete(config.urlApi + "/product-category/" + id,
      {
        withCredentials: true,
        // headers: new HttpHeaders()
        //   .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )

      .subscribe(
        res => {

          if (res) {
            success(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          console.log(err.status)
          if (err.status == 200) {
            success(true)
          } else {

            error(JSON.stringify(err.error.error));
          }


        });
  }
}
