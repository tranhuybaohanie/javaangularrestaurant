import { TestBed } from '@angular/core/testing';

import { ProductdaoService } from './productdao.service';

describe('ProductdaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductdaoService = TestBed.get(ProductdaoService);
    expect(service).toBeTruthy();
  });
});
