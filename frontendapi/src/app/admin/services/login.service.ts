import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import config from '../../config';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  login(email, password, result, error) {
    const body = new HttpParams()
      .set('email', email)
      .set('password', password);

    this.http.post<any>(config.urlApi + "/login",
      body.toString(),
      // {
      //   email:"bao1.@gmail.com",
      //   password:"abc"
      // },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            result(true)
          } else {
            error("Checking your email or password!");
          }

        },
        err => {
          error(JSON.stringify(err.error.text));

        }
      );
  }
  logout(result,error) {
  
    this.http.get<any>(config.urlApi + "/logout",
    
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            result(true)
          } else {
            error("Checking your email or password!");
          }

        },
        err => {
          error(JSON.stringify(err.error.text));

        }
      );
  }
  customerRegister(fullname,email, password, result, error) {
    // const body = new HttpParams()
    //   .set('email', email)
    //   .set('fullname', fullname)
    //   .set('password', password);

    this.http.post<any>(config.urlApi + "/signup",
     // body.toString(),
      {
        email,
        password,fullname
      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }


    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            result(true)
          } else {
            error("Checking your email or password!");
          }

        },
        err => {
          error(JSON.stringify(err.error.text));

        }
      );
  }
  isAuth(): Promise<boolean> {
    const body = new HttpParams()
      .set('email', "email")
      .set('password', "password");
    return new Promise(async resolve => {


      // resolve(true)
      await this.http.get(config.urlApi + "/auth",

        // {
        //   email:"bao1.@gmail.com",
        //   password:"abc"
        // },
        {
          withCredentials: true,
          headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        }


      )
        .toPromise()
        .then(res => {
          console.log(res)
          if (res != "false") {
            resolve(false);
          } else {
            resolve(true);
          }
        })
        .catch(err => {
          console.log(err.error)

          if (err.error.text && err.error.text.includes("@")) {
            resolve(true);
          }
          resolve(false);

        });





    })
  }
  isAuthEmail(re,er){
    
    
    this.http.get(config.urlApi + "/authemail",
     {
       withCredentials: true,
       headers: new HttpHeaders()
         .set('Content-Type', 'application/x-www-form-urlencoded')
     }


   )
     .toPromise()
     .then(res => {
       console.log(res)
     re(res)
     })
     .catch(err => {
     console.log(err)
         er(err)
     });





 
}
  isAuthName(re,er){
    
    
       this.http.get(config.urlApi + "/authname",
        {
          withCredentials: true,
          headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        }


      )
        .toPromise()
        .then(res => {
          console.log(res)
        re(res)
        })
        .catch(err => {
        console.log(err)
            er(err)
        });





    
  }
}