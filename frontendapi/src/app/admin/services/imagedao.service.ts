import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import config from '../../config';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
@Injectable({
  providedIn: 'root'
})
export class ImagedaoService {

  private serverUrl = config.urlApiSocket + '/socket'
  private title = 'WebSockets chat';
  private stompClient;
  private listImagesSnapshot;
  constructor(private http: HttpClient) {


  }
  initializeWebSocketConnection(result) {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/image/snapshot", (message) => {
        if (message.body) {
          // $(".chat").append("<div class='message'>"+message.body+"</div>")

          result(message.body);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send("/app/update/image", {}, message);
    // $('#input').val('');
  }
  //   getSnapShot(result){
  //     console.log("ddddddddddddddddddddddddddddddddddddddddd")
  //     this.initializeWebSocketConnection(resultS=>{console.log(resultS)});
  // result(this.listImagesSnapshot);
  //   }
  add(name, file, result, error) {
    // headers
    const headers = new HttpHeaders()
      .append('Content-Type', 'multipart/form-data');

    const formData: FormData = new FormData();
    formData.append('filedata', file, file.name);
    formData.append("name", name);
    this.http.post<any>(config.urlApi + "/image",
      formData
      // {
      //   // withCredentials: true,
      //   // headers: new HttpHeaders().set('Content-Type', 'application/form-data')
      // }
    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            // this.sendMessage("update");
            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          console.log(err.status)
          if (err.status == 200) {
            // this.sendMessage("update");
            result(true)
          } else {

            error(JSON.stringify(err.error.error));
          }


        }
      );

  }



  getAll(success, error) {
    this.http.get<any>(config.urlApi + "/image",
      {
        withCredentials: true,
        // headers: new HttpHeaders()
        //   .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )

      .subscribe(
        res => {

          success(res)


        },
        err => {
          console.log(err)
          if (err.status == 200) {
            success(true)
          } else {

            error(JSON.stringify(err.error.error));
          }


        }
      );



  }


  deleteById(id,success, error) {
    this.http.delete(config.urlApi + "/image/"+id,
      {
        withCredentials: true,
        // headers: new HttpHeaders()
        //   .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )

    .subscribe(
      res => {

        if (res) {
          this.sendMessage("update");
          success(true)
        } else {
          error("Some errors occured in processing!");
        }

      },
      err => {
        console.log(err.status)
        if (err.status == 200) {
          this.sendMessage("delete");
          success(true)
        } else {

          error(JSON.stringify(err.error.error));
        }


      });
  }

}
