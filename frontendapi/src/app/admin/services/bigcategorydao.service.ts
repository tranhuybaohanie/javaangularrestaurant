import { Injectable } from '@angular/core';
import { BigcategoryService } from '../model/bigcategory.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import config from '../../config';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
@Injectable({
  providedIn: 'root'
})
export class BigcategorydaoService {
  private serverUrl = config.urlApiSocket + '/socket'
  private title = 'WebSockets chat';
  private stompClient;
  private listImagesSnapshot;
  constructor(private http: HttpClient) {


  }
  initializeWebSocketConnection(result) {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/bigcategory/snapshot", (message) => {
        if (message.body) {
          // $(".chat").append("<div class='message'>"+message.body+"</div>")

          result(message.body);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send("/app/update/bigcategory", {}, message);
    // $('#input').val('');
  }

  addnew(bigcategory: BigcategoryService, result, error) {

    this.http.post<any>(config.urlApi + "/big-category",
      {
        nameVi: bigcategory.name || "",
        descriptionVi: bigcategory.description || "",
        status: bigcategory.status,
        imgUrl: bigcategory.img_url || "",
        metaKeyword: bigcategory.focus_keyword || "",
        metaDiscription: bigcategory.description_seo || "",
        metaTitle: bigcategory.meta_title || ""

      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            //  var pro= new Promise((rs,rj)=>{
            //   this.initializeWebSocketConnection(()=>{})
            //  })
            // pro.then(()=>{
            //   this.sendMessage("ad new")
            // })


            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }
  
  update(bigcategory: BigcategoryService, result, error) {

    this.http.put<any>(config.urlApi + "/big-category",
      {
        id:bigcategory.id,
        nameVi: bigcategory.name || "",
        descriptionVi: bigcategory.description || "",
        status: bigcategory.status,
        imgUrl: bigcategory.img_url || "",
        metaKeyword: bigcategory.focus_keyword || "",
        metaDiscription: bigcategory.description_seo || "",
        metaTitle: bigcategory.meta_title || ""

      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            //  var pro= new Promise((rs,rj)=>{
            //   this.initializeWebSocketConnection(()=>{})
            //  })
            // pro.then(()=>{
            //   this.sendMessage("ad new")
            // })


            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }
  getById(id, result, error) {

    this.http.get<any>(config.urlApi + "/big-category/" + id,
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {
          var bigcategory = new BigcategoryService();
          bigcategory.id = res.id;
          bigcategory.name = res.nameVi;
          bigcategory.description = res.descriptionVi;
          bigcategory.author = res.author;
          bigcategory.create_date = res.createDate;
          bigcategory.img_url = res.imgUrl;
          bigcategory.description_seo = res.metaDiscription;
          bigcategory.focus_keyword = res.metaKeyword;
          bigcategory.meta_title = res.metaTitle;
          bigcategory.status = res.status;

          result(bigcategory)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }

  getAll(result, error) {

    this.http.get<any>(config.urlApi + "/big-category",
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          result(res)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }


  deleteById(id, success, error) {
    this.http.delete(config.urlApi + "/big-category/" + id,
      {
        withCredentials: true,
        // headers: new HttpHeaders()
        //   .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )

      .subscribe(
        res => {

          if (res) {
            success(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          console.log(err.status)
          if (err.status == 200) {
            success(true)
          } else {

            error(JSON.stringify(err.error.error));
          }


        });
  }
}
