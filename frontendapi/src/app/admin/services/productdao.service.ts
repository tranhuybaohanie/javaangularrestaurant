import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import config from '../../config';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
import { ProductService } from '../model/product.service';
@Injectable({
  providedIn: 'root'
})
export class ProductdaoService {
  private serverUrl = config.urlApiSocket + '/socket'
  private title = 'WebSockets chat';
  private stompClient;
  private listImagesSnapshot;
  constructor(private http: HttpClient) {


  }
  initializeWebSocketConnection(result) {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/product/snapshot", (message) => {
        if (message.body) {
          // $(".chat").append("<div class='message'>"+message.body+"</div>")

          result(message.body);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send("/app/update/product", {}, message);
    // $('#input').val('');
  }

  addnew(item: ProductService, result, error) {
    this.http.post<any>(config.urlApi + "/product",
      {
        nameVi: item.name || "",
        descriptionVi: item.description || "",
        status: item.status,
        imgUrl: item.img_url || "",
        imgMoreUrl: item.img_more_url || "",
        metaKeyword: item.focus_keyword || "",
        metaDiscription: item.description_seo || "",
        metaTitle: item.meta_title || "",
        bigCategoryId: item.big_category_id,
        productCategoryId: item.product_category_id,
        currency: item.currency,
        nutritionalValue: item.nutritional_value,
        price: item.price,
        promotionPrice: item.promotion_price,
        ingredient: item.ingredient




      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {
          if (typeof res.result != "undefined" && res.result) {
            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          console.log(err)
          error(JSON.stringify(err.error.error));

        }
      );
  }

  update(item: ProductService, result, error) {

    this.http.put<any>(config.urlApi + "/product",
      {
        id: item.id,
        nameVi: item.name || "",
        descriptionVi: item.description || "",
        status: item.status,
        imgUrl: item.img_url || "",
        imgMoreUrl: item.img_more_url || "",
        metaKeyword: item.focus_keyword || "",
        metaDiscription: item.description_seo || "",
        metaTitle: item.meta_title || "",
        bigCategoryId: item.big_category_id,
        productCategoryId: item.product_category_id,
        currency: item.currency,
        nutritionalValue: item.nutritional_value,
        price: item.price,
        promotionPrice: item.promotion_price,
        ingredient: item.ingredient

      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            //  var pro= new Promise((rs,rj)=>{
            //   this.initializeWebSocketConnection(()=>{})
            //  })
            // pro.then(()=>{
            //   this.sendMessage("ad new")
            // })


            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }
  getById(id, result, error) {

    this.http.get<any>(config.urlApi + "/product/" + id,
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {
          var item = new ProductService();
          item.id = res.id;
          item.name = res.nameVi;
          item.description = res.descriptionVi;
          item.author = res.author;
          item.create_date = res.createDate;
          item.img_url = res.imgUrl;
          item.description_seo = res.metaDiscription;
          item.focus_keyword = res.metaKeyword;
          item.meta_title = res.metaTitle;
          item.big_category_id = res.bigCategoryId;
          item.status = res.status;
          item.ingredient = res.ingredient;
          item.price = res.price;
          item.nutritional_value = res.nutritionalValue;
          item.promotion_price = res.promotionPrice;
          item.product_category_id = res.productCategoryId;
          item.view_count=res.viewCount;
          item.currency=res.currency;
          item.img_more_url=res.imgMoreUrl;
         
          result(item)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }

  getByCategoryId(id, result, error) {

    this.http.get<any>(config.urlApi + "/product/category/" + id,
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
    .subscribe(
      res => {

        result(res)


      },
      err => {
        error(JSON.stringify(err.error.error));

      }
    );
  }

  getAll(result, error) {

    this.http.get<any>(config.urlApi + "/product",
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          result(res)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }


  deleteById(id, success, error) {
    this.http.delete(config.urlApi + "/product/" + id,
      {
        withCredentials: true,
        // headers: new HttpHeaders()
        //   .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )

      .subscribe(
        res => {

          if (res) {
            success(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          console.log(err.status)
          if (err.status == 200) {
            success(true)
          } else {

            error(JSON.stringify(err.error.error));
          }


        });
  }
}
