import { TestBed } from '@angular/core/testing';

import { OrderDaoService } from './order-dao.service';

describe('OrderDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderDaoService = TestBed.get(OrderDaoService);
    expect(service).toBeTruthy();
  });
});
