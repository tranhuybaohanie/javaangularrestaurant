import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import config from '../../config';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';
import { ProductService } from '../model/product.service';
import { OrderService } from '../model/order.service';
@Injectable({
  providedIn: 'root'
})
export class OrderDaoService {
  private serverUrl = config.urlApiSocket + '/socket'
  private title = 'WebSockets chat';
  private stompClient;
  private listImagesSnapshot;
  constructor(private http: HttpClient) {


  }
  initializeWebSocketConnection(result) {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe("/order/snapshot", (message) => {
        if (message.body) {
          // $(".chat").append("<div class='message'>"+message.body+"</div>")

          result(message.body);
        }
      });
    });
  }

  sendMessage(message) {
    this.stompClient.send("/app/update/order", {}, message);
    // $('#input').val('');
  }

  addnew(customerInfo, result, error) {
    var orderItemList = []
    JSON.parse(localStorage.getItem("cart")).map(item => {
      orderItemList.push({
        id: item.id,
        code: null,
        bigCategoryId: item.big_category_id,
        productCategoryId: item.product_category_id,
        price: item.price,
        viewCount: item.view_count,
        promotionPrice: item.promotion_price,
        currency: item.currency,
        ingredient: item.ingredient,
        nutritionalValue: item.nutritional_value,
        descriptionVi: item.description,
        createDate: item.create_date,
        author: item.author,
        nameEn: item.name,
        nameVi: item.name_en,
        metaKeyword: item.meta_keyword,
        metaDiscription: item.meta_description,
        status: item.status,
        imgUrl: item.img_url,
        imgMoreUrl: item.img_more_url,
        modifiedBy: item.modified_by,
        modifiedDate: item.modified_date,
        metaTitle: 1,
        quantity: parseInt(item.quantity)
      });
    })
    if (orderItemList.length > 0)
      this.http.post<any>(config.urlApi + "/order",
        {

          order: {
            fullName: customerInfo.fullName,
            address: customerInfo.address,
            phone: customerInfo.phone,
          },
          orderItem: orderItemList
        },
        {
          withCredentials: true,
          headers: new HttpHeaders()
            .set('Content-Type', 'application/json')
        }
      )
        .subscribe(
          res => {
            if (typeof res.result != "undefined" && res.result) {
              result(true)
            } else {
              error("Some errors occured in processing!");
            }

          },
          err => {
            console.log(err)
            error(JSON.stringify(err.error.error));

          }
        );
  }

  update(item: OrderService, key, result, error) {

    this.http.put<any>(config.urlApi + "/order",
      {
        order: {
          id: item.id,
          status: item.status,
          receivedState: item.receivedState,
          deliveringState: item.deliveringState,
          deliveredState: item.deliveredState
        },
        key

      },
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          if (typeof res.result != "undefined" && res.result) {
            //  var pro= new Promise((rs,rj)=>{
            //   this.initializeWebSocketConnection(()=>{})
            //  })
            // pro.then(()=>{
            //   this.sendMessage("ad new")
            // })


            result(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }
  getByUserEmail(email, result, error) {

    this.http.get<any>(config.urlApi + "/order/getbyuseremail/" + email,
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          result(res);


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }

  getOrderItemListByOrderId(id, result, error) {

    this.http.get<any>(config.urlApi + "/order-item/byorderid/" + id,
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          result(res)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }

  getAll(result, error) {

    this.http.get<any>(config.urlApi + "/order",
      {
        withCredentials: true,
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
      }
    )
      .subscribe(
        res => {

          result(res)


        },
        err => {
          error(JSON.stringify(err.error.error));

        }
      );
  }


  deleteById(id, success, error) {
    this.http.delete(config.urlApi + "/product/" + id,
      {
        withCredentials: true,
        // headers: new HttpHeaders()
        //   .set('Content-Type', 'application/x-www-form-urlencoded')
      }


    )

      .subscribe(
        res => {

          if (res) {
            success(true)
          } else {
            error("Some errors occured in processing!");
          }

        },
        err => {
          console.log(err.status)
          if (err.status == 200) {
            success(true)
          } else {

            error(JSON.stringify(err.error.error));
          }


        });
  }
}
