import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private login:LoginService,private router: Router){

  }
 
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    // / Observable<boolean> | 
    Promise<boolean> | boolean
      {
        // return true;
        this.login.isAuth().then(re=>{
          if(re==false){
          this.router.navigate(['/admin/login'], {
            queryParams: {
              return: state.url
            }
          });
        }
        })
         
        return this.login.isAuth();
   
    
  }
}
