import { Component, OnInit } from '@angular/core';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import $ from 'jquery';

@Component({
  selector: 'app-example-socket',
  templateUrl: './example-socket.component.html',
  styleUrls: ['./example-socket.component.scss']
})
export class ExampleSocketComponent implements OnInit {


    public serverUrl = 'http://localhost:8091/socket'
    public title = 'WebSockets chat';
    public stompClient;
  
    constructor(){
      this.initializeWebSocketConnection();
    }
    ngOnInit(){}
  
    initializeWebSocketConnection(){
      let ws = new SockJS(this.serverUrl);
      this.stompClient = Stomp.over(ws);
      let that = this;
      this.stompClient.connect({}, function(frame) {
        that.stompClient.subscribe("/image/snapshot", (message) => {
          if(message.body) {
           // $(".chat").append("<div class='message'>"+message.body+"</div>")
            console.log(message.body);
          }
        });
      });
    }
  
    sendMessage(message){
      this.stompClient.send("/app/update/image" , {}, message);
      $('#input').val('');
    }
  
  
}