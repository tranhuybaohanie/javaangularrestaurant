import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss',
  '../../../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
  '../../../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
  '../../../../assets/admindashboard/assets/vendor/linearicons/style.css',
  '../../../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
  '../../../../assets/admindashboard/assets/css/main.css',
  '../../../../assets/admindashboard/assets/css/demo.css'
]
})
export class SidebarComponent implements OnInit {
  show=this.router.url;
linkSideBar=[
  {
    link:"/admin/home",
    icon:"lnr-home",
    name:"Dashboard"
  },
  {
    link:"/admin/big-category",
    icon:"lnr-database",
    name:"Big Category"
  },
  {
    link:"/admin/product-category",
    icon:"lnr-book",
    name:"Product Category"
  },
  {
    link:"/admin/product",
    icon:"lnr-dinner",
    name:"Product"
  },

  {
    link:"/admin/order",
    icon:"lnr-store",
    name:"Orders"
  }
]
constructor(private router: Router) {
setInterval(()=>{
  this.show=router.url;

},500)
  
}

  ngOnInit() {
    
  }
  ngOnChanges(){
   
  }
 
}
