import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  totalPage;
  currentPage: number = -1;
  @Input() public originList;
  @Input() public session;
  @Input() public typeLifeCircleCheck
  @Input() public numberItemOnePage;
  @Output() changePage = new EventEmitter<boolean>();
  changePageNum(index) {

    this.currentPage = index;
    var from = this.currentPage * this.numberItemOnePage;
    var end = (this.currentPage + 1) * this.numberItemOnePage;
    if (typeof this.originList == "object") {
      console.log(this.originList.filter((item, id) => { return id >= from && id < end ? item : null }))
      this.changePage.emit(this.originList.filter((item, id) => { return id >= from && id < end ? item : null }));
    }
  }
  constructor() { }

  ngOnInit() { }
  ngOnChanges() {

    if (this.originList.length < this.numberItemOnePage) {
      this.totalPage = Array(1).fill(0).map((x, i) => i);;
    } else {
      var TotalItems = this.originList.length;
      var length = (TotalItems / this.numberItemOnePage) > Math.round(TotalItems / this.numberItemOnePage) ? Math.round(TotalItems / this.numberItemOnePage) + 1 : Math.round(TotalItems / this.numberItemOnePage);
   //   console.log(length + "   " + TotalItems / this.numberItemOnePage)
      this.totalPage = Array(length).fill(0).map((x, i) => i);

    }
    // if(this.currentPage<0&&this.originList.length>0)
    this.changePageNum(0);
  
  }
 

}
