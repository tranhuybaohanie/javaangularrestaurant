import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss',
  '../../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
  '../../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
  '../../../assets/admindashboard/assets/vendor/linearicons/style.css',
  '../../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
  '../../../assets/admindashboard/assets/css/main.css',
  '../../../assets/admindashboard/assets/css/demo.css'
]
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
