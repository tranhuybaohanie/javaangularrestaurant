import { Component, OnInit } from '@angular/core';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss',
  '../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
  '../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
  '../../assets/admindashboard/assets/vendor/linearicons/style.css',
  '../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
  '../../assets/admindashboard/assets/css/main.css',
  '../../assets/admindashboard/assets/css/demo.css'
],

})
export class AdminComponent implements OnInit {
  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'vi']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|vi/) ? browserLang : 'en');
  }
  ngOnInit() {
  }

}
