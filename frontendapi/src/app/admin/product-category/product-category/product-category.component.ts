import { Component, OnInit } from '@angular/core';
import { BigcategoryService } from '../../model/bigcategory.service';
import { BigcategorydaoService } from '../../services/bigcategorydao.service';
import { Router } from '@angular/router';
import { ProductcategorydaoService } from '../../services/productcategorydao.service';
import { ProductcategoryService } from '../../model/productcategory.service';



@Component({
  selector: 'app-product-category.',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.scss',
    '../../../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
    '../../../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
    '../../../../assets/admindashboard/assets/vendor/linearicons/style.css',
    '../../../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
    '../../../../assets/admindashboard/assets/css/main.css',
    '../../../../assets/admindashboard/assets/css/demo.css'
  ]
})
export class ProductCategoryComponent implements OnInit {
  itemList = [];
  numberItemOnePage = 5;
  pagedItems;
  deletedid;
  session = 0;
  constructor(public modelItem: ProductcategoryService, public itemDao: ProductcategorydaoService, public router: Router) { }
  public notification = {
    title: "sadsflasdkjfl",
    notificationType: "success animated bounce",
    bodyText: "",
    show: false
  }

  setNotification(title, type, body, show) {
    this.notification.title = title;
    this.notification.notificationType = type;
    this.notification.bodyText = body;
    this.notification.show = show;

  }
  confirm = {
    title: "ok",
    content: "content",
    show: false
  }
  setConfirm(title, content, show) {
    this.confirm.title = title;
    this.confirm.content = content;
    this.confirm.show = show;
  }
  delete(id) {

    this.deletedid = id;
    this.setConfirm("Confirm", "Do you really want to delete " + id, true);
  }
  yes() {
    this.itemDao.deleteById(this.deletedid,
      () => {
        this.setNotification("Congratulation", "success animated bounce", "You have deleted a item successful: ", true);
      },
      () => {
        this.setNotification("Fail", "danger", "Fail to delete", true);
      })
    this.setConfirm("Confirm", "Do you really want to delete ", false);
  }
  no() {
    this.setConfirm("Confirm", "Do you really want to delete ", false);
  }

  ngOnInit() {

    this.itemDao.initializeWebSocketConnection(result => {

      this.itemList = [];
      JSON.parse(result).map((item, idd) => {
        var itemNew = new ProductcategoryService();
        itemNew.id = item.id;
        itemNew.name = item.nameVi;
        itemNew.description = item.descriptionVi;
        itemNew.author = item.author;
        itemNew.create_date = item.createDate;
        itemNew.img_url = item.imgUrl;
        itemNew.description_seo = item.metaDiscription;
        itemNew.focus_keyword = item.metaKeyword;
        itemNew.meta_title = item.metaTitle;
        itemNew.status = item.status;
        this.itemList.push(itemNew)
        this.session = this.session + 1;
      })
    })
    // this.bigcategorydao.sendMessage("message")

    this.itemDao.getAll(result => {

      result.map((item, idd) => {
        var itemTe = new ProductcategoryService();
        //console.log(item)

        itemTe.id = item.id;
        itemTe.name = item.nameVi;
        itemTe.description = item.descriptionVi;
        itemTe.author = item.author;
        itemTe.create_date = item.createDate;
        itemTe.img_url = item.imgUrl;
        itemTe.description_seo = item.metaDiscription;
        itemTe.focus_keyword = item.metaKeyword;
        itemTe.meta_title = item.metaTitle;
        itemTe.status = item.status;
        this.itemList.push(itemTe)
        this.session = this.session + 1;
      })
    }, err => {
      console.log(err)
    })
  }
  updateItems(id) {
    this.router.navigate(['/admin/product-category/update'], {
      queryParams: {
        id: id
      }
    });
  }

}
