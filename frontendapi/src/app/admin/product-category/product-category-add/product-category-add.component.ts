  import { Component, OnInit } from '@angular/core';
  import { BigcategoryService } from '../../model/bigcategory.service';
  import { ProductcategoryService } from '../../model/productcategory.service';
  import { BigcategorydaoService } from '../../services/bigcategorydao.service';
import { ProductcategorydaoService } from '../../services/productcategorydao.service';
  
  @Component({
    selector: 'app-product-category-add',
    templateUrl: './product-category-add.component.html',
    styleUrls: ['./product-category-add.component.scss',
      // '../../../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
      '../../../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
      // '../../../../assets/admindashboard/assets/vendor/linearicons/style.css',
      // '../../../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
      // '../../../../assets/admindashboard/assets/css/main.css',
      // '../../../../assets/admindashboard/assets/css/demo.css'
    ]
  })
  export class ProductCategoryAddComponent implements OnInit {
    session;
    bigcategoryList;
    public notification={
      title:"sadsflasdkjfl",
      notificationType:"success animated bounce",
      bodyText:"",
      show:false
    }
    numberItemOnePage=1;
    public imgLibrary={
     returnUrl:"",
      show:false
    }
    setNotification(title,type,body,show){
      this.notification.title=title;
      this.notification.notificationType=type;
      this.notification.bodyText=body;
      this.notification.show=show;
    }
    showImgLibrary(){
      this.imgLibrary.show=true
    }
    constructor(public itemModel: ProductcategoryService, public itemDao: ProductcategorydaoService,public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService) {
  this.itemModel.status=true;
  
    }
    changeStatus(){
      this.itemModel.status=!this.itemModel.status;
    }
    getUrlLibrary(url){
      this.itemModel.img_url=url;
      this.imgLibrary.show=false;
    }
    submitForm() {
     
      this.itemDao.addnew(this.itemModel,
        success => {
          this.setNotification("Congratulation","success animated bounce","You have added a item successful: "+this.itemModel.name,true);
        },
        err => {
          this.setNotification("Fail","danger","Fail to add: "+this.itemModel.name,true);
        }
      );
  
    }
    ngOnInit() {
      this.bigcategorydao.getAll(result => {
        this.bigcategoryList=[];
        result.map((item,idd)=>{
          var bigcategory= new BigcategoryService();
          //console.log(item)
     
        bigcategory.id = item.id;
        bigcategory.name = item.nameVi;
        bigcategory.description = item.descriptionVi;
        bigcategory.author = item.author;
        bigcategory.create_date = item.createDate;
        bigcategory.img_url = item.imgUrl;
        bigcategory.description_seo = item.metaDiscription;
        bigcategory.focus_keyword = item.metaKeyword;
        bigcategory.meta_title = item.metaTitle;
        bigcategory.status = item.status;
        this.bigcategoryList.push(bigcategory)
        this.session=this.session+1;
      })
      }, err => {
        console.log(err)
      })
    }
  
  }
  

