import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,NgModel } from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { BigcategoryComponent } from './big-category/bigcategory/bigcategory.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { LoginService } from './services/login.service';
import { AddnewComponent } from './big-category/addnew/addnew.component';
import { NotificationComponent } from './notification/notification.component';
import { ConfirmFormComponent } from './confirm-form/confirm-form.component';
import { ImageLibraryComponent } from './image-library/image-library.component';
import { PaginationComponent } from './shared/pagination/pagination.component';
import { ExampleSocketComponent } from './example-socket/example-socket.component';
import { UpdateComponent } from './big-category/update/update.component';
import { ProductCategoryAddComponent } from './product-category/product-category-add/product-category-add.component';
import { ProductCategoryUpdateComponent } from './product-category/product-category-update/product-category-update.component';
import { ProductCategoryComponent } from './product-category/product-category/product-category.component';
import { ProductComponent } from './product/product/product.component';
import { ProductAddComponent } from './product/product-add/product-add.component';
import { ProductUpdateComponent } from './product/product-update/product-update.component';
import { MenuComponent } from './menu/menu/menu.component';
import { MenuAddComponent } from './menu/menu-add/menu-add.component';
import { MenuUpdateComponent } from './menu/menu-update/menu-update.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
  AdminComponent,
    HomeComponent,
    SidebarComponent,
    BigcategoryComponent,
    LoginComponent,
    AddnewComponent,
    NotificationComponent,
    ConfirmFormComponent,
    ImageLibraryComponent,
    PaginationComponent,
    ExampleSocketComponent,
    UpdateComponent,
    ProductCategoryAddComponent,
    ProductCategoryUpdateComponent,
    ProductCategoryComponent,
    ProductComponent,
    ProductAddComponent,
    ProductUpdateComponent,
    MenuComponent,
    MenuAddComponent,
    MenuUpdateComponent,
    OrderComponent,
    OrderDetailComponent,
    
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    AdminRoutingModule,
    FormsModule,
    HttpClientModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [LoginService],
  bootstrap: [AdminComponent]
})

export class AdminModule { }
