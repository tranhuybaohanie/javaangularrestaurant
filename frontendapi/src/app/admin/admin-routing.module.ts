import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { BigcategoryComponent } from './big-category/bigcategory/bigcategory.component';
import {LoginComponent} from './login/login.component'
import { AuthGuard } from './guard/auth.guard';
import { AddnewComponent as BigcategoryAdnew} from './big-category/addnew/addnew.component';
import { ExampleSocketComponent } from './example-socket/example-socket.component';
import { UpdateComponent as BigcategoryUpdate} from './big-category/update/update.component';
import { ProductCategoryComponent } from './product-category/product-category/product-category.component';
import { ProductCategoryAddComponent } from './product-category/product-category-add/product-category-add.component';
import { ProductCategoryUpdateComponent } from './product-category/product-category-update/product-category-update.component';
import { ProductUpdateComponent } from './product/product-update/product-update.component';
import { ProductComponent } from './product/product/product.component';
import { ProductAddComponent } from './product/product-add/product-add.component';
import { OrderComponent } from './order/order.component';

const routes: Routes = [
  {path:'socket',component:ExampleSocketComponent},
  { path: 'admin/login', component: LoginComponent},
  { path: 'admin', component: AdminComponent,
  canActivate:[AuthGuard],
  
children:[
  // { path: 'login', component: LoginComponent},
  { path: '',  pathMatch: 'full',redirectTo:"/admin/home" },
  {path:'home',component:HomeComponent},
  {path:'big-category/update',component:BigcategoryUpdate},
  {path:'big-category',component:BigcategoryComponent},
  { path: 'big-category/add-new', component: BigcategoryAdnew},
  {path:'product-category',component:ProductCategoryComponent},
  { path: 'product-category/add-new', component: ProductCategoryAddComponent},
  {path:'product-category/update',component:ProductCategoryUpdateComponent},
  {path:'product',component:ProductComponent},
  { path: 'product/add-new', component: ProductAddComponent},
  {path:'product/update',component:ProductUpdateComponent},
  {path:'order',component:OrderComponent},
]
},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
