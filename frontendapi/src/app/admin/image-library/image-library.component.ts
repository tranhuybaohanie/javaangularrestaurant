import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ImagedaoService } from '../services/imagedao.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-image-library',
  templateUrl: './image-library.component.html',
  styleUrls: ['./image-library.component.scss']
})
export class ImageLibraryComponent implements OnInit {
  imgFile;
  imgName;
  imageList;
  deletedid;
  numberItemOnePage = 10;
  typeLifeCircleCheck='ngOnChanges'
  pagedItems;
 classOutForm='img-form hide';
  @Input() public show: boolean = true;
  @Output() getUrlLibrary = new EventEmitter<boolean>();
  @Output() close = new EventEmitter<boolean>();
  closeBtn = () => {

    this.close.emit(false)
    this.classOutForm='img-form p-3 animated slideOutDown';
  }
  selectImgClick = (e) => {
    this.getUrlLibrary.emit(e.target.src)
  }
  public notification = {
    title: "sadsflasdkjfl",
    notificationType: "success animated bounce",
    bodyText: "",
    show: false
  }

  setNotification(title, type, body, show) {
    this.notification.title = title;
    this.notification.notificationType = type;
    this.notification.bodyText = body;
    this.notification.show = show;

  }
  confirm = {
    title: "ok",
    content: "content",
    show: false
  }
  setConfirm(title, content, show) {
    this.confirm.title = title;
    this.confirm.content = content;
    this.confirm.show = show;
  }
  yes() {
    this.imagedao.deleteById(this.deletedid,
      () => {
        this.setNotification("Congratulation", "success animated bounce", "You have deleted a item successful: ", true);
      },
      () => {
        this.setNotification("Fail", "danger", "Fail to delete", true);
      })
    this.setConfirm("Confirm", "Do you really want to delete ", false);
  }
  no() {
    this.setConfirm("Confirm", "Do you really want to delete ", false);
  }
  constructor(public imagedao: ImagedaoService) {

  }
  fotmatProtocal(item) {

    return !item.includes("http") ? 'http://' + item : item;
  }
  ngOnInit() {
    this.imagedao.getAll(success => {
      this.imageList = success;
    }, err => {
      console.log(err)
    })
    this.imagedao.initializeWebSocketConnection(result => {
      console.log(JSON.parse(result))
      this.imageList = JSON.parse(result);
    })

  }
  deleteImage(id) {
    this.deletedid = id;
    this.setConfirm("Confirm", "Do you really want to delete " + id, true);
  }
  onFileChanged(event) {
    this.imgFile = event.target.files[0]
  }
  upload() {
    this.imagedao.add(this.imgName, this.imgFile, success => {
      this.setNotification("Congratulation", "success animated bounce", "You have added a item successful: " + this.imgName, true);
    },
      err => {
        this.setNotification("Fail", "danger", "Fail to add: " + this.imgName, true);


      })

  }
}
