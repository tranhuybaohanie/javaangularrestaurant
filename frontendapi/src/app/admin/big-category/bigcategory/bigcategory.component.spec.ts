import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BigcategoryComponent } from './bigcategory.component';

describe('BigcategoryComponent', () => {
  let component: BigcategoryComponent;
  let fixture: ComponentFixture<BigcategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BigcategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BigcategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
