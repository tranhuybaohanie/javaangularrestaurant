import { Component, OnInit } from '@angular/core';
import { BigcategoryService } from '../../model/bigcategory.service';
import { BigcategorydaoService } from '../../services/bigcategorydao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bigcategory',
  templateUrl: './bigcategory.component.html',
  styleUrls: ['./bigcategory.component.scss',
    '../../../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
    '../../../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
    '../../../../assets/admindashboard/assets/vendor/linearicons/style.css',
    '../../../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
    '../../../../assets/admindashboard/assets/css/main.css',
    '../../../../assets/admindashboard/assets/css/demo.css'
  ]
})
export class BigcategoryComponent implements OnInit {
     bigcategoryList=[];
     numberItemOnePage = 5;
     pagedItems;
     deletedid;
     session=0;
  constructor(public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService,public router: Router) { }
  public notification = {
    title: "sadsflasdkjfl",
    notificationType: "success animated bounce",
    bodyText: "",
    show: false
  }

  setNotification(title, type, body, show) {
    this.notification.title = title;
    this.notification.notificationType = type;
    this.notification.bodyText = body;
    this.notification.show = show;

  }
  confirm = {
    title: "ok",
    content: "content",
    show: false
  }
  setConfirm(title, content, show) {
    this.confirm.title = title;
    this.confirm.content = content;
    this.confirm.show = show;
  }
  delete(id) {
    
    this.deletedid = id;
    this.setConfirm("Confirm", "Do you really want to delete " + id, true);
  }
  yes() {
    this.bigcategorydao.deleteById(this.deletedid,
      () => {
        this.setNotification("Congratulation", "success animated bounce", "You have deleted a item successful: ", true);
      },
      () => {
        this.setNotification("Fail", "danger", "Fail to delete", true);
      })
    this.setConfirm("Confirm", "Do you really want to delete ", false);
  }
  no() {
    this.setConfirm("Confirm", "Do you really want to delete ", false);
  }

  ngOnInit() {
   
    this.bigcategorydao.initializeWebSocketConnection(result => {

      this.bigcategoryList=[];
      JSON.parse(result).map((item,idd)=>{
        var bigcategory= new BigcategoryService();
        //console.log(item)
   
      bigcategory.id = item.id;
      bigcategory.name = item.nameVi;
      bigcategory.description = item.descriptionVi;
      bigcategory.author = item.author;
      bigcategory.create_date = item.createDate;
      bigcategory.img_url = item.imgUrl;
      bigcategory.description_seo = item.metaDiscription;
      bigcategory.focus_keyword = item.metaKeyword;
      bigcategory.meta_title = item.metaTitle;
      bigcategory.status = item.status;
      this.bigcategoryList.push(bigcategory)
      this.session=this.session+1;
    })
    })
    // this.bigcategorydao.sendMessage("message")

    this.bigcategorydao.getAll(result => {
     
      result.map((item,idd)=>{
        var bigcategory= new BigcategoryService();
        //console.log(item)
   
      bigcategory.id = item.id;
      bigcategory.name = item.nameVi;
      bigcategory.description = item.descriptionVi;
      bigcategory.author = item.author;
      bigcategory.create_date = item.createDate;
      bigcategory.img_url = item.imgUrl;
      bigcategory.description_seo = item.metaDiscription;
      bigcategory.focus_keyword = item.metaKeyword;
      bigcategory.meta_title = item.metaTitle;
      bigcategory.status = item.status;
      this.bigcategoryList.push(bigcategory)
      this.session=this.session+1;
    })
    }, err => {
      console.log(err)
    })
  }
  updateItems(id){
    this.router.navigate(['/admin/big-category/update'], {
      queryParams: {
        id: id
      }
    });
  }

}
