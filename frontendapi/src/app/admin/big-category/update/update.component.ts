import { Component, OnInit } from '@angular/core';
import { BigcategoryService } from '../../model/bigcategory.service';
import { BigcategorydaoService } from '../../services/bigcategorydao.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-addnew',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss',
    // '../../../../assets/admindashboard/assets/vendor/bootstrap/css/bootstrap.min.css',
    '../../../../assets/admindashboard/assets/vendor/font-awesome/css/font-awesome.min.css',
    // '../../../../assets/admindashboard/assets/vendor/linearicons/style.css',
    // '../../../../assets/admindashboard/assets/vendor/chartist/css/chartist-custom.css',
    // '../../../../assets/admindashboard/assets/css/main.css',
    // '../../../../assets/admindashboard/assets/css/demo.css'
  ]
})
export class UpdateComponent implements OnInit {
  updateId;
  public notification = {
    title: "sadsflasdkjfl",
    notificationType: "success animated bounce",
    bodyText: "",
    show: false
  }
  numberItemOnePage = 1;
  public imgLibrary = {
    returnUrl: "",
    show: false
  }
  setNotification(title, type, body, show) {
    this.notification.title = title;
    this.notification.notificationType = type;
    this.notification.bodyText = body;
    this.notification.show = show;
  }
  showImgLibrary() {
    this.imgLibrary.show = true
  }
  constructor(public bigcategory: BigcategoryService, public bigcategorydao: BigcategorydaoService, public activatedRoute: ActivatedRoute) {
    this.bigcategory.status = true;
    this.activatedRoute.queryParams.subscribe(params => {
      this.updateId = params['id'];
    });
  }

  changeStatus() {    this.bigcategory.status = !this.bigcategory.status;
  }

  getUrlLibrary(url) {
    this.bigcategory.img_url = url;
    this.imgLibrary.show = false;
  }
  submitForm() {

    this.bigcategorydao.update(this.bigcategory,
      success => {
        this.setNotification("Congratulation", "success animated bounce", "You have added a item successful: " + this.bigcategory.name, true);
      },
      err => {
        this.setNotification("Fail", "danger", "Fail to add: " + this.bigcategory.name, true);
      }
    );

  }
  ngOnInit() {
    this.bigcategorydao.getById(this.updateId, success => {
      this.bigcategory=success;
    },
      err => {
        this.setNotification("Fail", "danger", "Fail to add: " + this.bigcategory.name, true);
      }
    )
  }

}
