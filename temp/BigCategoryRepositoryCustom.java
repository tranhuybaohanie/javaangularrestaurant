package com.example.api.dao;

import java.util.Date;

public interface BigCategoryRepositoryCustom {

	long getMaxId();
	long updateUserStaff(String empNo, String fullName, Date hireDate);

}