package com.example.api.dao;

import java.util.Date;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.example.api.model.UserStaff;

@Repository
public class UserStaffRepositoryCustomImpl implements UserStaffRepositoryCustom {

   @Autowired
   MongoTemplate mongoTemplate;

   /* (non-Javadoc)
 * @see com.example.api.dao.UserStaffRepositoryCustom#getMaxUserStaffId()
 */
@Override
public long getMaxUserStaffId() {
       Query query = new Query();
       query.with(new Sort(Sort.Direction.DESC, "id"));
       query.limit(1);
       UserStaff maxObject = (UserStaff) mongoTemplate.findOne(query, UserStaff.class);
       if (maxObject == null) {
           return 0L;
       }
       return maxObject.getId();
   }

   /* (non-Javadoc)
 * @see com.example.api.dao.UserStaffRepositoryCustom#updateUserStaff(java.lang.String, java.lang.String, java.util.Date)
// */
//@Override
//   public long updateUserStaff(String empNo, String fullName, Date hireDate) {
//       Query query = new Query(Criteria.where("empNo").is(empNo));
//       Update update = new Update();
//       update.set("fullName", fullName);
//       update.set("hireDate", hireDate);
//
//       UpdateResult result = this.mongoTemplate.updateFirst(query, update, UserStaff.class);
//
//       if (result != null) {
//           return result.getModifiedCount();
//       }
//
//       return 0;
//   }

}