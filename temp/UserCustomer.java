package com.example.api.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
 
@Document(collection = "user_customer")
public class UserCustomer {
 
    @Id
    private Long id;
 
    @Indexed(unique = true)
    @Field(value = "code")
    private String userCustomerNo;
 
    @Field(value = "full_name")
    private String fullName;
 
    @Field(value = "create_date")
    private Date createDate;
    
    @Field(value = "address")
    private String adress ;
    
    @Field(value = "phone")
    private String phone ;
    
    @Field(value = "user_name")
    private String userName;
    
    @Field(value = "password")
    private String password;
    
    @Field(value = "email")
    private String email;
    
    public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getUserCustomerNo() {
		return userCustomerNo;
	}


	public void setUserCustomerNo(String userCustomerNo) {
		this.userCustomerNo = userCustomerNo;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public Date getCreateDate() {
		return createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getAdress() {
		return adress;
	}


	public void setAdress(String adress) {
		this.adress = adress;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getImgUrl() {
		return imgUrl;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	@Field(value = "img_url")
    private String imgUrl;  
  
   
    @Override
    public String toString() {
        return "id:" + this.id + ", empNo: " + userCustomerNo //
                + ", fullName: " + this.fullName + ", hireDate: " + this.createDate;
    }
}