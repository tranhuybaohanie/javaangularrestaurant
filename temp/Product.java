package com.example.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
 
@Document(collection = "product")
public class Product {
 
    @Id
    private Long id;
 
    @Indexed(unique = true)
    @Field(value = "code")
    private String code;
 
    @Field(value = "description_en")
    private String description_en;
    
    @Field(value = "product_category_code")
    private String productCategoryCode;
    
    @Field(value = "big_category_code")
    private String bigCategoryCode;
    
    @Field(value = "img_more")
    private String imgMore;
    
    public String getProductCategoryCode() {
		return productCategoryCode;
	}

	public void setProductCategoryCode(String productCategoryCode) {
		this.productCategoryCode = productCategoryCode;
	}

	public String getBigCategoryCode() {
		return bigCategoryCode;
	}

	public void setBigCategoryCode(String bigCategoryCode) {
		this.bigCategoryCode = bigCategoryCode;
	}

	public String getImgMore() {
		return imgMore;
	}

	public void setImgMore(String imgMore) {
		this.imgMore = imgMore;
	}

	public String getIngredient_en() {
		return ingredient_en;
	}

	public void setIngredient_en(String ingredient_en) {
		this.ingredient_en = ingredient_en;
	}

	public String getIngredient_vi() {
		return ingredient_vi;
	}

	public void setIngredient_vi(String ingredient_vi) {
		this.ingredient_vi = ingredient_vi;
	}

	public String getNutritional_vi() {
		return nutritional_vi;
	}

	public void setNutritional_vi(String nutritional_vi) {
		this.nutritional_vi = nutritional_vi;
	}

	public String getNutritional_en() {
		return nutritional_en;
	}

	public void setNutritional_en(String nutritional_en) {
		this.nutritional_en = nutritional_en;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getPromotion_price() {
		return promotion_price;
	}

	public void setPromotion_price(Integer promotion_price) {
		this.promotion_price = promotion_price;
	}

	public Integer getView() {
		return view;
	}

	public void setView(Integer view) {
		this.view = view;
	}

	@Field(value = "ingredient_en")
    private String ingredient_en;
    
    @Field(value = "ingredient_vi")
    private String ingredient_vi;
    
    @Field(value = "nutritional_vi")
    private String nutritional_vi;
    
    @Field(value = "nutritional_en")
    private String nutritional_en;
    
    @Field(value = "price")
    private Integer price;
    
    @Field(value = "promotion_price")
    private Integer promotion_price;
   
    @Field(value = "view")
    private Integer view;
   
    @Field(value = "description_vi")
    private String description_vi;
 
    @Field(value = "create_date")
    private String createDate;
    
    @Field(value = "img_url")
    private String imgUrl;
    
    @Field(value = "modified_by")
    private String modifiedBy; 
    
    @Field(value = "modified_date")
    private String modifiedDate; 
    
    @Field(value = "name_en")
    private String name_en ;
    
    @Field(value = "name_vi")
    private String name_vi;
    
    @Field(value = "author")
    private String author ;
    
    
    @Field(value = "focus_keyword")
    private String focusKeyword;
    
    @Field(value = "seo_title")
    private String seoTitle;
    
    @Field(value = "meta_description")
    private String metaDescription;
    
    @Field(value = "slug")
    private String slug;

    @Field(value = "status")
    private Boolean status;
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription_en() {
		return description_en;
	}

	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}

	public String getDescription_vi() {
		return description_vi;
	}

	public void setDescription_vi(String description_vi) {
		this.description_vi = description_vi;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getName_en() {
		return name_en;
	}

	public void setName_en(String name_en) {
		this.name_en = name_en;
	}

	public String getName_vi() {
		return name_vi;
	}

	public void setName_vi(String name_vi) {
		this.name_vi = name_vi;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}


	public String getFocusKeyword() {
		return focusKeyword;
	}

	public void setFocusKeyword(String focusKeyword) {
		this.focusKeyword = focusKeyword;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}


	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}


    
    
 
//    @Override
//    public String toString() {
//        return "id:" + this.id + ", empNo: " + this.userStaffNo //
//                + ", fullName: " + this.fullName + ", hireDate: " + this.createDate;
//    }
}