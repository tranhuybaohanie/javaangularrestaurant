package com.example.api.dao;


import java.util.Date;
import java.util.List;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.example.api.model.Employee;
import com.example.api.model.UserStaff;

//This is an Interface.
//No need Annotation here
public interface UserStaffRepository extends MongoRepository<UserStaff, Long> { // Long: Type of Employee ID.

	UserStaff findByEmail(String email);

   List<UserStaff> findByFullNameLike(String fullName);

   List<UserStaff> findByCreateDateGreaterThan(Date createDate);

//   // Supports native JSON query string
//   @Query("{fullName:'?0'}")
//   List<UserStaff> findCustomByFullName(String fullName);

}
