package com.example.api.controllers;


import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.api.dao.EmployeeRepository;
import com.example.api.dao.EmployeeRepositoryCustom;
import com.example.api.dao.UserStaffRepository;
import com.example.api.dao.UserStaffRepositoryCustom;
import com.example.api.model.Employee;
import com.example.api.model.UserStaff;
import com.example.api.utilities.DateTime;

@Controller

@RequestMapping("/api/user-staff")
public class UserStaffController {
//
//   private static final String[] NAMES = { "Tom", "Jerry", "Donald" };
//
//   @Autowired
//   private UserStaffRepositoryCustom userStaffRepositoryCustom;
//
//   @Autowired
//   private UserStaffRepository userStaffRepository;
//
////   @ResponseBody
////   @RequestMapping("/")
////   public String home() {
////       String html = "";
////       html += "<ul>";
////       html += " <li><a href='/api/userstaff/testInsert'>Test Insert</a></li>";
////       html += " <li><a href='/api/userstaff/showAllEmployee'>Show All Employee</a></li>";
////       html += " <li><a href='/api/userstaff/showFullNameLikeTom'>Show All 'Tom'</a></li>";
////       html += " <li><a href='/api/userstaff/deleteAllEmployee'>Delete All Employee</a></li>";
////       html += "</ul>";
////       return html;
////   }
//
//   @ResponseBody
//   @RequestMapping(value ="/", method = RequestMethod.POST)
//   public String Insert(@RequestBody UserStaff userStaff) {
////       UserStaff staff = new UserStaff();
////
////       long id = this.userStaffRepositoryCustom.getMaxUserStaffId() + 1;
////       int idx = (int) (id % NAMES.length);	
////       String fullName = NAMES[idx] + " " + id;
////       staff.setId(id);
////       staff.setUserStaffNo(DateTime.getCurrentDateTimeForID()+"S" + id);
////       staff.setFullName(fullName);
////       staff.setCreateDate(DateTime.getCurrentDateTime());
////       this.userStaffRepository.insert(staff);
//
//       return "Inserted: " + userStaff.getFullName();
//   }
//
//   @ResponseBody
//   @RequestMapping(value ="/", method = RequestMethod.GET)
//   public String showAllEmployee() {
//       List<UserStaff> userStaff = this.userStaffRepository.findAll();
//
//       String html = "";
//       for (UserStaff emp : userStaff) {
//           html += emp + "<br>";
//       }
//
//       return html;
//   }
//
//   @ResponseBody
//   @RequestMapping("/showFullNameLikeTom")
//   public String showFullNameLikeTom() {
//
//	   List<UserStaff> userStaff = this.userStaffRepository.findByFullNameLike("Tom");
//
//       String html = "";
//       for (UserStaff emp : userStaff) {
//           html += emp + "<br>";
//       }
//
//       return html;
//   }
//
//   @ResponseBody
//   @RequestMapping("/deleteAllEmployee")
//   public String deleteAllEmployee() {
//
//       this.userStaffRepository.deleteAll();
//       return "Deleted!";
//   }

}