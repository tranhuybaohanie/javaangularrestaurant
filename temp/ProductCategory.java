package com.example.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
 
@Document(collection = "product_category")
public class ProductCategory {
 
    @Id
    private Long id;
 
    @Indexed(unique = true)
    @Field(value = "code")
    private String code;
 
    @Field(value = "description_en")
    private String description_en;
   
    @Field(value = "description_vi")
    private String description_vi;
 
    @Field(value = "create_date")
    private String createDate;
    
    @Field(value = "img_url")
    private String imgUrl;
    
    @Field(value = "modified_by")
    private String modifiedBy; 
    
    @Field(value = "modified_date")
    private String modifiedDate; 
    
    @Field(value = "name_en")
    private String name_en ;
    
    @Field(value = "name_vi")
    private String name_vi;
    
    @Field(value = "author")
    private String author ;
    
    @Field(value = "password")
    private String password;
    
    @Field(value = "focus_keyword")
    private String focusKeyword;
    
    @Field(value = "seo_title")
    private String seoTitle;
    
    @Field(value = "meta_description")
    private String metaDescription;
    
    @Field(value = "slug")
    private String slug;
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription_en() {
		return description_en;
	}

	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}

	public String getDescription_vi() {
		return description_vi;
	}

	public void setDescription_vi(String description_vi) {
		this.description_vi = description_vi;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getName_en() {
		return name_en;
	}

	public void setName_en(String name_en) {
		this.name_en = name_en;
	}

	public String getName_vi() {
		return name_vi;
	}

	public void setName_vi(String name_vi) {
		this.name_vi = name_vi;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFocusKeyword() {
		return focusKeyword;
	}

	public void setFocusKeyword(String focusKeyword) {
		this.focusKeyword = focusKeyword;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Field(value = "email")
    private String email;
    
    @Field(value = "status")
    private Boolean status;
    
 
//    @Override
//    public String toString() {
//        return "id:" + this.id + ", empNo: " + this.userStaffNo //
//                + ", fullName: " + this.fullName + ", hireDate: " + this.createDate;
//    }
}