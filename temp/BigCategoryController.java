package com.example.api.controllers;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.api.dao.BigCategoryRepository;
import com.example.api.dao.BigCategoryRepositoryCustom;
import com.example.api.dao.EmployeeRepository;
import com.example.api.dao.EmployeeRepositoryCustom;
import com.example.api.dao.UserStaffRepository;
import com.example.api.dao.UserStaffRepositoryCustom;
import com.example.api.model.BigCategory;
import com.example.api.model.Employee;
import com.example.api.model.UserStaff;
import com.example.api.utilities.DateTime;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Controller

@RequestMapping("/api/big-category")
public class BigCategoryController {

   private static final String[] NAMES = { "Tom", "Jerry", "Donald" };

//   @Autowired
//   private BigCategoryRepositoryCustom bigCategoryRepositoryCustom;
//
//   @Autowired
//   private BigCategoryRepository bigCategoryRepository;


//   @ResponseBody
//   @RequestMapping("/")
//   public String home() {
//       String html = "";
//       html += "<ul>";
//       html += " <li><a href='/api/userstaff/testInsert'>Test Insert</a></li>";
//       html += " <li><a href='/api/userstaff/showAllEmployee'>Show All Employee</a></li>";
//       html += " <li><a href='/api/userstaff/showFullNameLikeTom'>Show All 'Tom'</a></li>";
//       html += " <li><a href='/api/userstaff/deleteAllEmployee'>Delete All Employee</a></li>";
//       html += "</ul>";
//       return html;
//   }

//   @ResponseBody
//   @RequestMapping(value ="/", method = RequestMethod.POST, produces = "application/json")
//   public HashMap<String, Object> Insert(@RequestBody BigCategory bigCategoryPost) {
//      try {
//	   BigCategory bigCategory = new BigCategory();
//       long id = this.bigCategoryRepositoryCustom.getMaxId() + 1;
//       bigCategory.setId(id);
//       bigCategory.setCode(DateTime.getCurrentDateTimeForID()+"S" + id);
//       bigCategory.setAuthor(bigCategoryPost.getAuthor());
//       bigCategory.setDescriptionVi(bigCategoryPost.getDescriptionVi());
//       bigCategory.setDescriptionEn(bigCategoryPost.getDescriptionEn());
//       bigCategory.setNameEn(bigCategoryPost.getNameEn());
//       bigCategory.setNameVi(bigCategoryPost.getNameVi());
//       bigCategory.setStatus(bigCategoryPost.getStatus());
//       bigCategory.setCreateDate(DateTime.getCurrentDateTime());
//       this.bigCategoryRepository.insert(bigCategory);
//
//       HashMap<String, Object> map = new HashMap<>();
//       map.put("result", true);
//       return map;
//      }catch(Exception e) {
//    	  HashMap<String, Object> map = new HashMap<>();
//          map.put("result", false);
//          map.put("error",e);
//          return map;
//      }
//   }
//
//   @ResponseBody
//   @RequestMapping(value ="/", method = RequestMethod.GET, produces = "application/json")
//   public  List<BigCategory> showAll() {
//       List<BigCategory> bigCategory = this.bigCategoryRepository.findAll();
//
////       String html = "";
////       for (UserStaff emp : userStaff) {
////           html += emp + "<br>";
////       }
//
//       return bigCategory;
//   }

//   @ResponseBody
//   @RequestMapping("/showFullNameLikeTom")
//   public String showFullNameLikeTom() {
//
//	   List<UserStaff> userStaff = this.userStaffRepository.findByFullNameLike("Tom");
//
//       String html = "";
//       for (UserStaff emp : userStaff) {
//           html += emp + "<br>";
//       }
//
//       return html;
//   }
//
//   @ResponseBody
//   @RequestMapping("/deleteAllEmployee")
//   public String deleteAllEmployee() {
//
//       this.userStaffRepository.deleteAll();
//       return "Deleted!";
//   }

}